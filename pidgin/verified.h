#ifndef VERIFIED_H
#define VERIFIED_H

/* config.h may define PURPLE_PLUGINS; protect the definition here so that we
 * don't get complaints about redefinition when it's not necessary. */
#ifndef PURPLE_PLUGINS
# define PURPLE_PLUGINS
#endif

#include <glib.h>

/* This will prevent compiler errors in some instances and is better explained in the
 * how-to documents on the wiki */
#ifndef G_GNUC_NULL_TERMINATED
# if __GNUC__ >= 4
#  define G_GNUC_NULL_TERMINATED __attribute__((__sentinel__))
# else
#  define G_GNUC_NULL_TERMINATED
# endif
#endif

#include <notify.h>
#include <gtkplugin.h>
#include <version.h>
#include <util.h>
#include <gtkconv.h>

/* we're adding this here and assigning it in plugin_load because we need
 * a valid plugin handle for our call to purple_notify_message() in the
 * plugin_action_test_cb() callback function */
PurplePlugin *verified_plugin = NULL;
guint update_db_timer = 0;
guint update_plugin_timer = 0;
guint update_news_timer = 0;
GList *forum_list = NULL;

typedef struct
{
    gchar *jid;
    gchar *url;
} Scammer;

GList *scammer_list = NULL;

typedef struct
{
    gchar *jid;
    gchar *login;
    gchar *registered;
    int id;
} GoodUser;

GList *good_user_list = NULL;

void free_scammer(Scammer *sc);
void free_good_user(GoodUser *gu);

gboolean update_plugin();
gboolean update_db();
void parse_db_forum(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message);
void parse_db_scammer(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message);
void parse_db_good_user(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message);
gboolean check_forum(PurpleAccount *account, char **who, char **message, PurpleConversation *conv, int *flags);

void check_scammers_in_history();
GList *contacts_in_history();

gboolean check_news();
void parse_news(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message);

void parse_plugin_update(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message);
void install_update(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message);

void update_conversation(PurpleConversation *conv, PurpleConvUpdateType type);
void update_alert(PidginConversation *conv);
void destroy_alert(PidginConversation *conv);

int show_ask_update(int new_version);
void show_update_done();
void show_self_scammer(gchar *blacks);
void show_wrong_plugin_path(gchar *correct_path);

#endif // VERIFIED_H
