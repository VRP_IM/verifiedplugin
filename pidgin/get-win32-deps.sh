#!/usr/bin/bash

set -x
set -e

[ -d win32 ] && rm -fr win32

mkdir win32
pushd win32

mkdir win32-dev
pushd win32-dev

mkdir gtk_2_0-2.14
pushd gtk_2_0-2.14
wget http://ftp.gnome.org/pub/gnome/binaries/win32/gtk+/2.14/gtk+-bundle_2.14.7-20090119_win32.zip
unzip -x gtk+-bundle_2.14.7-20090119_win32.zip
popd # gtk_2_0-2.14


mkdir gettext-0.17
pushd gettext-0.17
wget http://ftp.gnome.org/pub/gnome/binaries/win32/dependencies/gettext-tools-0.17.zip
wget http://ftp.gnome.org/pub/gnome/binaries/win32/dependencies/gettext-runtime-0.17-1.zip
unzip -x gettext-tools-0.17.zip
unzip -x gettext-runtime-0.17-1.zip
chmod a+x bin/*.exe
popd # gettext-0.17

wget https://developer.pidgin.im/static/win32/libxml2-2.9.2_daa1.tar.gz
tar xzf libxml2-2.9.2_daa1.tar.gz

wget https://developer.pidgin.im/static/win32/perl-5.20.1.1.tar.gz
tar xzf perl-5.20.1.1.tar.gz

wget https://developer.pidgin.im/static/win32/gtkspell-2.0.16.tar.bz2
tar xjf gtkspell-2.0.16.tar.bz2

wget https://developer.pidgin.im/static/win32/enchant_1.6.0_win32.zip
unzip -x enchant_1.6.0_win32.zip

wget https://developer.pidgin.im/static/win32/nss-3.24-nspr-4.12.tar.gz
tar xzf nss-3.24-nspr-4.12.tar.gz

wget https://developer.pidgin.im/static/win32/silc-toolkit-1.1.12.tar.gz
tar xzf silc-toolkit-1.1.12.tar.gz

wget https://developer.pidgin.im/static/win32/meanwhile-1.0.2_daa3-win32.zip
unzip -x meanwhile-1.0.2_daa3-win32.zip

wget https://developer.pidgin.im/static/win32/cyrus-sasl-2.1.26_daa1.tar.gz
tar xzf cyrus-sasl-2.1.26_daa1.tar.gz

wget http://ftp.acc.umu.se/pub/GNOME/binaries/win32/intltool/0.40/intltool_0.40.4-1_win32.zip

wget https://developer.pidgin.im/static/win32/pidgin-inst-deps-20130214.tar.gz
tar xzf pidgin-inst-deps-20130214.tar.gz

popd # win32-dev

wget http://downloads.sourceforge.net/pidgin/pidgin-2.13.0.tar.bz2
tar xjf pidgin-2.13.0.tar.bz2

pushd pidgin-2.13.0

patch -p1 <<'EOF'
diff --git a/libpurple/win32/global.mak b/libpurple/win32/global.mak
index 08b8a55..565a5ea 100644
--- a/libpurple/win32/global.mak
+++ b/libpurple/win32/global.mak
@@ -101,7 +101,7 @@ DLL_LD_FLAGS += -Wl,--enable-auto-image-base -Wl,--enable-auto-import $(LD_HARDE
 ifeq "$(origin CC)" "default"
   CC := gcc.exe
 endif
-GMSGFMT ?= $(WIN32_DEV_TOP)/gettext-0.17/bin/msgfmt
+GMSGFMT ?= $(WIN32_DEV_TOP)/gettext-0.17/bin/msgfmt.exe
 MAKENSIS ?= makensis.exe
 PERL ?= perl
 WINDRES ?= windres
EOF

mingw32-make -f Makefile.mingw

popd # pidgin-2.13.0

popd # win32
