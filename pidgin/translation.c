#include "translation.h"

#include <locale.h>
#include <glib.h>

enum {
    EN = 1,
    RU
};

int lang = 0;

static const gchar *get_text_en(int id)
{
    switch (id) {
    case NONASCII:
        return
"<b>Attention!</b> "
"You are communcating with person who uses NON-Latin characters in his Jabber ID. "
"Usually scammers do this. Read here <font color=\"blue\">/showthread.php?t=7336</font> (Verified forum) for details.";
        break;

    case LIKE_USERNAME:
        return
"<b>Attention!</b> "
"You are communcating with person whose username looks like a Jabber ID but not identical with his real Jabber ID. Be careful!";
        break;

    case SCAMMER:
        return
"<b>Attention!</b> "
"You are communcating with person who has blacks. Read here for details:";
        break;

    case FORUM:
        return
"<span style=\"font-size: large;\">"
"<span style=\"color: red;font-weight: bold;\">Attention!</span> "
"Just now you or your parthner send a link which contained in our fake forums database. Be careful! "
"To known more open Verified forum and type in search box this URL."
"</span>";
        break;

    case SELF_BLACK:
        return
"<b><span color=\"red\">Attention!</span></b> "
"You got a BlackList. Please respond to it. Link to topic:";
        break;

    case NEW_VERSION_TITLE:
        return
"<big>A new version of Verified Plugin is available.</big>";
        break;

    case NEW_VERSION_BODY:
        return
"Verified Plugin %d is now available (you have %d). Would you like to download it now?<br>"
"See changelog here: <a href=\"http://vrp.im\">http://vrp.im</a>";
        break;

    case SKIP_VERSION:
        return
"Skip this version";
        break;

    case REMIND_LATER:
        return
"Remind me later";
        break;

    case INSTALL_UPDATE:
        return
"Install update";
        break;

    case PLUGIN_WARNING:
        return
"Verified Plugin Warning";
        break;

    case PLUGIN_ERROR:
        return
"Verified Plugin Error";
        break;

    case PLUGIN_UPDATE:
        return
"Verified Plugin Update";
        break;
    return "";

    case WRONG_PATH:
        return
"Current path %s.\nMust be at %s.";
        break;

    case NEED_RESTART:
        return
"Installed a new version.\nNeed to restart application.";
        break;

    case PLUGIN_SUMMARY:
        return
"Protect from scammers. See vrp.im for details.";
        break;

    case NEWS_TITLE:
        return
"Verified message";
        break;

    case GOOD_USER:
        return
"<span style=\"font-size: large;\">"
"<span style=\"color: green;font-weight: bold;\">Attention!</span> You are communicating with person who has registration on Verified forum<br>"
"Login: %s<br>"
"Registration date: %s<br>"
"Link to profile: <font color=\"blue\">/member.php?u=%d</font></big>"
"</span>";
        break;

    case VERIFIED_FORUM:
        return
"Verified forum";
        break;

    case HISTORY_SCAMMER_DESCRIPTION:
        return
"In last 30 days you had conversation with persons who get blacks.\n";
        break;
    }
    return "";
}

static const gchar *get_text_ru(int id)
{
    switch (id) {
    case NONASCII:
        return
"<b>Внимание!</b> "
"Вы общаетесь с человеком который использует в своем Jabber ID НЕ латинские буквы. "
"Обычно такое делают кидалы, подробнее о данном виде мошенничества можно прочитать тут: <font color=\"blue\">/showthread.php?t=7336</font> (ресурс Verified).";
        break;

    case LIKE_USERNAME:
        return
"<b>Внимание!</b> "
"Вы общаетесь с человеком у которого никнейм в виде Jabber ID не соответсвтует его реальному Jabber ID. Будьте осторожны!";
        break;

    case SCAMMER:
        return
"<b>Внимание!</b> "
"Вы общаетесь с человеком на которого есть блеки. Подробнее об этом вы можете прочитать тут:";
        break;

    case FORUM:
        return
"<span style=\"font-size: large;\">"
"<span style=\"color: red;font-weight: bold;\">Внимание!</span> "
"Только что вы или ваш собеседник указали ссылку, которая фигурирует в нашей базе фейковых форумов. Будьте осторожны! "
"Для более подробной информации о данном фейке зайдите на ресурс Verified и вбейте в поиск данный URL."
"</span>";
        break;

    case SELF_BLACK:
        return
"<b><span color=\"red\">Внимание!</span></b> "
"На вас написан BlackList. Пожалуйста отреагируйте на него, ссылка на топик:";
        break;

    case NEW_VERSION_TITLE:
        return
"<big>Доступна новая версия Verified Plugin!</big>";
        break;

    case NEW_VERSION_BODY:
        return
"Verified Plugin %d доступен для загрузки (у вас - %d). Загрузить обновление сейчас?<br>"
"Смотреть информацию об обновлении здесь: <a href=\"http://vrp.im\">http://vrp.im</a>";
        break;

    case SKIP_VERSION:
        return
"Пропустить версию";
        break;

    case REMIND_LATER:
        return
"Не сейчас";
        break;

    case INSTALL_UPDATE:
        return
"Установить";
        break;

    case PLUGIN_WARNING:
        return
"Verified Plugin Предупреждение";
        break;

    case PLUGIN_ERROR:
        return
"Verified Plugin Ошибка";
        break;

    case PLUGIN_UPDATE:
        return
"Verified Plugin Обновление";
        break;

    case WRONG_PATH:
        return
"Текущий путь %s.\nНо должен быть %s.";
        break;

    case NEED_RESTART:
        return
"Установлена новая версия.\nНужно перезапустить приложение.";
        break;

    case PLUGIN_SUMMARY:
        return
"Защита от кидал. Подробнее на vrp.im.";
        break;

    case NEWS_TITLE:
        return
"Verified сообщение";
        break;

    case GOOD_USER:
        return
"<span style=\"font-size: large;\">"
"<span style=\"color: green;font-weight: bold;\">Внимание!</span> Вы общаетесь с человеком который зарегестрирован на ресурсе Verified<br>"
"Логин: %s<br>"
"Дата регистрации: %s<br>"
"Ссылка на профайл: <font color=\"blue\">/member.php?u=%d</font></big>"
"</span>";
        break;

    case VERIFIED_FORUM:
        return
"ресурс Verified";
        break;

    case HISTORY_SCAMMER_DESCRIPTION:
        return
"За последние 30 дней вы общались с людьми на которых есть блеки.\n";
        break;
    }
    return "";
}

const gchar *get_text(int id)
{
    if (!lang) {
        gchar *l = setlocale(LC_ALL, NULL);
        if (!g_ascii_strcasecmp(l, "russian") || !g_ascii_strcasecmp(l, "ru") || !g_ascii_strncasecmp(l, "ru-", 3) || !g_ascii_strncasecmp(l, "ru_", 3)
            || !g_ascii_strncasecmp(l, "russian_", 8) || !g_ascii_strncasecmp(l, "russian_", 8)) {

            lang = RU;
        }
        else {
            lang = EN;
        }
    }

    if (lang == EN) {
        return get_text_en(id);
    }
    else {
        return get_text_ru(id);
    }
}
