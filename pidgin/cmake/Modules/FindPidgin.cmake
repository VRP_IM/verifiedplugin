# - Try to find Pidgin Development Files
# Once done this will define
#  PIDGIN_FOUND - System has Pidgin Development Files
#  PIDGIN_INCLUDE_DIRS - The Pidgin Development Files include directories
#  PIDGIN_LIBRARIES - The libraries needed to use Pidgin Development Files
#  PIDGIN_DEFINITIONS - Compiler switches required for using Pidgin Development Files

find_package(PkgConfig)
pkg_check_modules(PC_PIDGIN pidgin QUIET)
set(PC_PIDGIN_DEFINITIONS ${PC_PIDGIN_CFLAGS_OTHER})

find_path(
    PIDGIN_INCLUDE_DIR
    NAMES
        pidgin.h
    HINTS
        ${PIDGIN_SOURCES}
        ${CMAKE_FIND_ROOT_PATH}
        ${PC_PIDGIN_INCLUDEDIR}
        ${PC_PIDGIN_INCLUDE_DIRS}
    PATH_SUFFIXES
        pidgin
    NO_DEFAULT_PATH
    CMAKE_FIND_ROOT_PATH_BOTH
)

if(WIN32)
    find_path(
        PIDGIN_WIN32_INCLUDE_DIR
        NAMES
            gtkwin32dep.h
        HINTS
            ${PIDGIN_SOURCES}
            ${CMAKE_FIND_ROOT_PATH}
            ${PC_PIDGIN_LIBDIR}
            ${PC_PIDGIN_LIBRARY_DIRS}
        PATH_SUFFIXES
            pidgin/win32
        NO_DEFAULT_PATH
        CMAKE_FIND_ROOT_PATH_BOTH
    )

    find_library(
        PIDGIN_LIBRARY
        NAMES pidgin
        HINTS
            ${PIDGIN_SOURCES}
            ${CMAKE_FIND_ROOT_PATH}
            ${PC_PIDGIN_LIBDIR}
            ${PC_PIDGIN_LIBRARY_DIRS}
        PATH_SUFFIXES
            pidgin
        NO_DEFAULT_PATH
        CMAKE_FIND_ROOT_PATH_BOTH
    )

    set(PIDGIN_LIBRARIES ${PIDGIN_LIBRARY})
    set(PIDGIN_INCLUDE_DIRS ${PIDGIN_INCLUDE_DIR} ${PIDGIN_WIN32_INCLUDE_DIR})
else()
    set(PIDGIN_INCLUDE_DIRS ${PIDGIN_INCLUDE_DIR})
endif()

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PIDGIN_FOUND to TRUE
# if all listed variables are TRUE
if(WIN32)
    find_package_handle_standard_args(Pidgin DEFAULT_MSG
        PIDGIN_LIBRARY PIDGIN_INCLUDE_DIR PIDGIN_WIN32_INCLUDE_DIR
    )
else()
    find_package_handle_standard_args(Pidgin DEFAULT_MSG
        PIDGIN_INCLUDE_DIR
    )
endif()

mark_as_advanced(PIDGIN_INCLUDE_DIR PIDGIN_WIN32_INCLUDE_DIR PIDGIN_LIBRARY)
