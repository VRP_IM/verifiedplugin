#!/usr/bin/bash

set -x
set -e

[ -d win32-build ] && rm -fr win32-build
mkdir win32-build
pushd win32-build

mingw32-cmake -DCMAKE_BUILD_TYPE=Release -DPIDGIN_SOURCES=`readlink -f ../win32/pidgin-2.13.0/` ..
make -j5
cp verified.dll "$HOME/.wine/drive_c/Program Files (x86)/Pidgin/plugins"

popd # win32-build
