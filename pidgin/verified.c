#include "verified.h"

#include "translation.h"

#include <pidgin.h>
#include <string.h>
#include <json.h>
#include <gtkconv.h>
#include <gtkimhtml.h>
#include <account.h>
#include <notify.h>
#include <gtkutils.h>
#include <gtknotify.h>
#include <gtkutils.h>

#include <glib/gstdio.h>

#define VERSION 15
#define VERSION_STR "15"

#if defined _WIN32
# define SYSTEM "windows"
# define LIB_NAME "verified.dll"
#else
# define SYSTEM "linux"
# define LIB_NAME "verified.so"
#endif

#if defined __x86_64 || defined __x86_64__ || defined __amd64 || defined _M_X64
# define ARCHITECTURE "x64"
#elif defined __i386  || defined __i386__ || defined _M_IX86
# define ARCHITECTURE "x32"
#endif

#define INI_GROUP "pidgin-" SYSTEM "-" ARCHITECTURE

#define SELF_BL_INTERVAL (24 * 60 * 60) /* 1 day */
#define UPDATE_DB_INTERVAL (3 * 60 * 60 * 1000) /* 3 hours */
#define UPDATE_PLUGIN_INTERVAL (24 * 60 * 60 * 1000) /* 1 day */
#define CHECK_NEWS_INTERVAL (10 * 60 * 1000) /* 10 minutes */
#define FORUM_DB_URL "http://vrp.im/db/db_forum.json"
#define NEWS_URL "http://vrp.im/db/message.json"
#define SCAMMER_DB_URL "http://vrp.im/db/db.json"
#define UPDATE_URL "http://vrp.im/psi-verified-appcast.ini"
#define GOOD_USER_DB_URL "http://vrp.im/db/db_goodusers.json"

#define UNUSED(x) (void)(x)

#if !GLIB_CHECK_VERSION(2, 40, 0)
gboolean g_str_is_ascii(const gchar *str)
{
    gint i;

    for (i = 0; str[i]; i++) {
        if (str[i] & 0x80) {
            return FALSE;
        }
    }

    return TRUE;
}
#endif

#if !GLIB_CHECK_VERSION(2, 28, 0)
void g_list_free_full(GList *list, GDestroyNotify  free_func)
{
    g_list_foreach(list, (GFunc) free_func, NULL);
    g_list_free(list);
}
#endif

static gboolean plugin_load(PurplePlugin *plugin)
{
    // Check path first
    GString *correct_path = g_string_new(purple_user_dir());
    correct_path = g_string_append(correct_path,  G_DIR_SEPARATOR_S "plugins" G_DIR_SEPARATOR_S LIB_NAME);

    if (g_strcmp0(correct_path->str, plugin->path)) {
        show_wrong_plugin_path(correct_path->str);
        return FALSE;
    }

    void *gtk_conv_handle = pidgin_conversations_get_handle();
    void *conv_handle = purple_conversations_get_handle();
    update_db_timer = g_timeout_add(UPDATE_DB_INTERVAL, update_db, NULL);
    update_plugin_timer = g_timeout_add(UPDATE_PLUGIN_INTERVAL, update_plugin, NULL);
    update_news_timer = g_timeout_add(CHECK_NEWS_INTERVAL, check_news, NULL);

    purple_signal_connect(conv_handle, "writing-im-msg", plugin, PURPLE_CALLBACK(check_forum), NULL);
    purple_signal_connect(gtk_conv_handle, "conversation-displayed", plugin, PURPLE_CALLBACK(update_alert), NULL);
    purple_signal_connect(conv_handle, "conversation-updated", plugin, PURPLE_CALLBACK(update_conversation), NULL);

    update_db();
    update_plugin();
    check_news();

    GList *convs = purple_get_conversations();
    while (convs) {
        PurpleConversation *conv = (PurpleConversation *)convs->data;

        if (PIDGIN_IS_PIDGIN_CONVERSATION(conv)) {
            update_alert(PIDGIN_CONVERSATION(conv));
        }

        convs = convs->next;
    }

    return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
    g_source_remove(update_db_timer);
    g_source_remove(update_plugin_timer);
    g_source_remove(update_news_timer);

    void *gtk_conv_handle = pidgin_conversations_get_handle();
    void *conv_handle = purple_conversations_get_handle();

    purple_signal_disconnect(conv_handle, "writing-im-msg", plugin, PURPLE_CALLBACK(check_forum));
    purple_signal_disconnect(gtk_conv_handle, "conversation-displayed", plugin, PURPLE_CALLBACK(update_alert));
    purple_signal_disconnect(conv_handle, "conversation-updated", plugin, PURPLE_CALLBACK(update_conversation));

    GList *convs = purple_get_conversations();
    while (convs) {
        PurpleConversation *conv = (PurpleConversation *)convs->data;

        if (PIDGIN_IS_PIDGIN_CONVERSATION(conv)) {
            destroy_alert(PIDGIN_CONVERSATION(conv));
        }

        convs = convs->next;
    }

    return TRUE;
}

static PurplePluginInfo info = {
    PURPLE_PLUGIN_MAGIC,
    PURPLE_MAJOR_VERSION,
    PURPLE_MINOR_VERSION,
    PURPLE_PLUGIN_STANDARD,
    PIDGIN_PLUGIN_TYPE,
    0,
    NULL,
    PURPLE_PRIORITY_DEFAULT,

    "gtk-vrp-verified",
    "Verified",
    VERSION_STR,

    NULL,
    NULL,
    NULL,
    "http://vrp.im/",


    plugin_load,
    plugin_unload,
    NULL,

    NULL,
    NULL,
    NULL,
    NULL,

    NULL,
    NULL,
    NULL,
    NULL
};

static void init_plugin(PurplePlugin *plugin)
{
    verified_plugin = plugin;
    plugin->info->description = (char*)get_text(PLUGIN_SUMMARY);
    plugin->info->summary = (char*)get_text(PLUGIN_SUMMARY);

    purple_prefs_add_none("/plugins/gtk/verified");
    purple_prefs_add_string_list("/plugins/gtk/verified/self-black-list", NULL);
    purple_prefs_add_string_list("/plugins/gtk/verified/history-blacks", NULL);
    purple_prefs_add_string("/plugins/gtk/verified/news-hashes", NULL);
    purple_prefs_add_int("/plugins/gtk/verified/skip-version", 0);
}

gboolean update_db()
{
    purple_util_fetch_url(FORUM_DB_URL, TRUE, "", TRUE, parse_db_forum, NULL);
    purple_util_fetch_url(SCAMMER_DB_URL, TRUE, "", TRUE, parse_db_scammer, NULL);
    purple_util_fetch_url(GOOD_USER_DB_URL, TRUE, "", TRUE, parse_db_good_user, NULL);

    return TRUE;
}

void parse_db_forum(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message)
{
    UNUSED(url_data);
    UNUSED(user_data);
    UNUSED(error_message);
    UNUSED(len);

    // Error
    if (!url_text) {
        return;
    }

    size_t i = 0;
    json_object *jobj = json_tokener_parse(url_text);

    if (!jobj) {
        return;
    }

    for (i = 0; i < json_object_array_length(jobj); ++i) {
        json_object *job = json_object_array_get_idx(jobj, i);
        json_object *type_ob;
        json_object *name_ob;
        json_object *data_ob;

        json_object_object_get_ex(job, "type", &type_ob);
        json_object_object_get_ex(job, "name", &name_ob);

        if (strcmp(json_object_get_string(type_ob), "table") || strcmp(json_object_get_string(name_ob), "rippers")) {
            continue;
        }

        g_list_free_full(forum_list, g_free);
        forum_list = NULL;

        json_object_object_get_ex(job, "data", &data_ob);

        for (size_t j = 0; j < json_object_array_length(data_ob); ++j) {
            json_object *forum_ob = json_object_array_get_idx(data_ob, j);
            forum_ob = json_object_object_get(forum_ob, "l");
            if (!forum_ob) {
                continue;
            }

            forum_list = g_list_prepend(forum_list, g_strdup(json_object_get_string(forum_ob)));
        }
    }

    json_object_put(jobj);
}

void parse_db_scammer(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message)
{
    UNUSED(url_data);
    UNUSED(user_data);
    UNUSED(error_message);
    UNUSED(len);

    // Error
    if (!url_text) {
        return;
    }

    json_object *jobj = json_tokener_parse(url_text);

    if (!jobj) {
        return;
    }

    for (size_t i = 0; i < json_object_array_length(jobj); ++i) {
        json_object *job = json_object_array_get_idx(jobj, i);
        json_object *type_ob;
        json_object *name_ob;
        json_object *data_ob;

        json_object_object_get_ex(job, "type", &type_ob);
        json_object_object_get_ex(job, "name", &name_ob);

        if (strcmp(json_object_get_string(type_ob), "table") || strcmp(json_object_get_string(name_ob), "rippers")) {
            continue;
        }

        g_list_free_full(scammer_list, (GDestroyNotify)free_scammer);
        scammer_list = NULL;

        json_object_object_get_ex(job, "data", &data_ob);

        for (size_t j = 0; j < json_object_array_length(data_ob); ++j) {
            json_object *scammer_ob = json_object_array_get_idx(data_ob, j);
            json_object *jid_ob = json_object_object_get(scammer_ob, "j");
            json_object *thread_ob = json_object_object_get(scammer_ob, "t");
            if (!jid_ob || !thread_ob) {
                continue;
            }

            Scammer *scammer = g_new(Scammer, 1);
            scammer->jid = g_strdup(json_object_get_string(jid_ob));
            scammer->url = g_strdup_printf("<font color=\"blue\">/showthread.php?t=%s</font>", json_object_get_string(thread_ob));
            scammer_list = g_list_prepend(scammer_list, scammer);
        }
    }

    json_object_put(jobj);

    // Update all tabs
    GList *convs = purple_get_conversations();
    while (convs) {
        PurpleConversation *conv = (PurpleConversation *)convs->data;

        if (PIDGIN_IS_PIDGIN_CONVERSATION(conv)) {
            update_alert(PIDGIN_CONVERSATION(conv));
        }

        convs = convs->next;
    }

    // Check self black
    GList *old_self_black_list = purple_prefs_get_string_list("/plugins/gtk/verified/self-black-list");
    GList *accounts = purple_accounts_get_all();
    GList *self_black_list = NULL;
    while (accounts) {
        PurpleAccount *acc = (PurpleAccount*)accounts->data;
        gchar *user = g_strdup(acc->username);
        for (gchar *ch = user; *ch; ++ch) {
            if (*ch == '/') {
                *ch = '\0';
                break;
            }
        }

        accounts = accounts->next;

        GString *scammer_urls = NULL;
        for (GList *l = scammer_list; l != NULL; l = l->next) {
            Scammer *scammer = (Scammer*)l->data;
            if (!g_strcmp0(user, scammer->jid)) {
                if (!scammer_urls) {
                    scammer_urls = g_string_new("");
                }

                scammer_urls = g_string_append(scammer_urls, "<br>");
                scammer_urls = g_string_append(scammer_urls, scammer->url);
            }
        }

        if (!scammer_urls) {
            continue;
        }

        int  count = 0;
        time_t old_time = 0;
        gchar *old_sb_string = NULL;
        for (GList *l = old_self_black_list; l != NULL; l = l->next) {
            int l_count;
            long long l_time;
            char l_jid[500];

            sscanf(l->data, "%d:%lld:%s", &l_count, &l_time, l_jid);

            if (!g_strcmp0(l_jid, user)) {
                old_sb_string = l->data;
                count = l_count;
                old_time = l_time;
                break;
            }
        }

        if (count >= 2 || time(NULL) - old_time < SELF_BL_INTERVAL) {
            if (old_sb_string) {
                self_black_list = g_list_prepend(self_black_list, g_strdup(old_sb_string));
            }
            continue;
        }

        scammer_urls = g_string_prepend(scammer_urls, get_text(SELF_BLACK));

        show_self_scammer(scammer_urls->str);
        g_string_free(scammer_urls, TRUE);

        GString *str = g_string_new("");
        g_string_append_printf(str, "%d:%lld:%s", ++count, (long long)time(NULL), user);
        self_black_list = g_list_prepend(self_black_list, str->str);
        g_string_free(str, FALSE);
        g_free(user);
    }

    purple_prefs_set_string_list("/plugins/gtk/verified/self-black-list", self_black_list);
    g_list_free_full(self_black_list, g_free);
    g_list_free_full(old_self_black_list, g_free);

    check_scammers_in_history();
}

void check_scammers_in_history()
{
    GList *history_blacks = purple_prefs_get_string_list("/plugins/gtk/verified/history-blacks");

    GString *message = g_string_new("");

    GList *contacts = contacts_in_history();

    for (GList *it = contacts; it != NULL; it = it->next) {
        const gchar *contact = it->data;
        gboolean added = FALSE;
        gboolean found = FALSE;

        for (GList *l = history_blacks; l != NULL; l = l->next) {
            if (!g_strcmp0(l->data, contact)) {
                added = TRUE;
                break;
            }
        }

        if (added) {
            continue;
        }

        history_blacks = g_list_prepend(history_blacks, g_strdup(contact));
        for (GList *it2 = scammer_list; it2 != NULL; it2 = it2->next) {
            Scammer *scammer = it2->data;
            if (g_strcmp0(contact, scammer->jid)) {
                continue;
            }

            found = TRUE;
            if (!added) {
                added = TRUE;
                g_string_append(message, "<b>");
                g_string_append(message, contact);
                g_string_append(message, "</b>");
            }

            g_string_append(message, "<br>");
            g_string_append(message, scammer->url);
        }

        if (found) {
            g_string_append(message, "<hr>");
        }
    }

    purple_prefs_set_string_list("/plugins/gtk/verified/history-blacks", history_blacks);

    g_list_free_full(history_blacks, g_free);
    g_list_free_full(contacts, g_free);

    // Show dialog with scammers in history
    if (message->len > 0) {
        GtkLabel *description = GTK_LABEL(gtk_label_new(get_text(HISTORY_SCAMMER_DESCRIPTION)));
        gtk_label_set_line_wrap_mode(description, PANGO_WRAP_WORD);
        gtk_label_set_single_line_mode(description, FALSE);

        GtkIMHtml *label = GTK_IMHTML(gtk_imhtml_new(NULL, NULL));
        gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(label), GTK_WRAP_NONE);
        gtk_imhtml_append_text_with_images(label, message->str, GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_SCROLL, NULL);

        GtkDialog *dlg = GTK_DIALOG(gtk_dialog_new());
        gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
        gtk_dialog_add_button(dlg, GTK_STOCK_CLOSE, 1);
        gtk_window_set_title(GTK_WINDOW(dlg), get_text(NEWS_TITLE));

        GtkVBox *content_area = GTK_VBOX(gtk_dialog_get_content_area(dlg));
        gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(description), TRUE, TRUE, 0);
        gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(label), TRUE, TRUE, 0);

        pidgin_auto_parent_window(GTK_WIDGET(dlg));
        gtk_widget_show_all(GTK_WIDGET(dlg));

        gtk_dialog_run(GTK_DIALOG(dlg));
        gtk_widget_destroy(GTK_WIDGET(dlg));
    }
}

GList *contacts_in_history()
{
    GString *history_path = g_string_new(purple_user_dir());
    GList *res = NULL;

    history_path = g_string_append(history_path,  G_DIR_SEPARATOR_S "logs" G_DIR_SEPARATOR_S "jabber");

    GDir *history_dir = g_dir_open(history_path->str, 0, NULL);
    time_t now = time(NULL);

    if (history_dir) {
        GString *account_path = g_string_new("");
        const gchar *account;
        while ((account = g_dir_read_name(history_dir))) {
            printf("%s\n", account);

            g_string_printf(account_path, "%s%c%s", history_path->str, G_DIR_SEPARATOR, account);

            GDir *account_dir = g_dir_open(account_path->str, 0, NULL);

            if (account_dir) {
                const gchar *contact;
                GString *contact_path = g_string_new("");
                while ((contact = g_dir_read_name(account_dir))) {

                    g_string_printf(contact_path, "%s%c%s", account_path->str, G_DIR_SEPARATOR, contact);
                    GDir *contact_dir = g_dir_open(contact_path->str, 0, NULL);

                    if (contact_dir) {
                        gchar *log_file;
                        while ((log_file = g_strdup(g_dir_read_name(contact_dir)))) {
                            if (purple_str_has_suffix(log_file, ".html") || purple_str_has_suffix(log_file, ".txt")) {
                                log_file[10] = '\0';
                                time_t log_time = purple_str_to_time(log_file, TRUE, NULL, NULL, NULL);

                                if (now - log_time >= 0 && now - log_time < 30 * 24 * 60 * 60 /* 30 days */) {
                                    res = g_list_prepend(res, g_strdup(contact));
                                    g_free(log_file);
                                    break;
                                }
                            }
                            g_free(log_file);
                        }
                    }
                }

                g_string_free(contact_path, TRUE);
            }
        }

        g_string_free(account_path, TRUE);
    }

    g_string_free(history_path, TRUE);

    return res;
}

void parse_db_good_user(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message)
{
    UNUSED(url_data);
    UNUSED(user_data);
    UNUSED(error_message);
    UNUSED(len);

    // Error
    if (!url_text) {
        return;
    }

    json_object *jobj = json_tokener_parse(url_text);

    if (!jobj) {
        return;
    }

    for (size_t i = 0; i < json_object_array_length(jobj); ++i) {
        json_object *job = json_object_array_get_idx(jobj, i);
        json_object *type_ob;
        json_object *name_ob;
        json_object *data_ob;

        json_object_object_get_ex(job, "type", &type_ob);
        json_object_object_get_ex(job, "name", &name_ob);

        if (strcmp(json_object_get_string(type_ob), "table") || strcmp(json_object_get_string(name_ob), "goodusers")) {
            continue;
        }

        g_list_free_full(good_user_list, (GDestroyNotify)free_good_user);
        good_user_list = NULL;

        json_object_object_get_ex(job, "data", &data_ob);

        for (size_t j = 0; j < json_object_array_length(data_ob); ++j) {
            json_object *gu_ob = json_object_array_get_idx(data_ob, j);
            json_object *jid_ob = json_object_object_get(gu_ob, "j");
            json_object *id_ob = json_object_object_get(gu_ob, "i");
            json_object *login_ob = json_object_object_get(gu_ob, "l");
            json_object *registered_ob = json_object_object_get(gu_ob, "r");

            if (!jid_ob || !id_ob || !login_ob || !registered_ob) {
                continue;
            }

            GoodUser *gu = g_new(GoodUser, 1);
            gu->jid = g_strdup(json_object_get_string(jid_ob));
            gu->login = g_strdup(json_object_get_string(login_ob));
            gu->registered = g_strdup(json_object_get_string(registered_ob));
            gu->id = json_object_get_int(id_ob);

            // to lower
            for (int i = 0; gu->jid[i]; ++i) {
                gu->jid[i] = g_ascii_tolower(gu->jid[i]);
            }

            good_user_list = g_list_prepend(good_user_list, gu);
        }
    }

    json_object_put(jobj);

    // Update all tabs
    GList *convs = purple_get_conversations();
    while (convs) {
        PurpleConversation *conv = (PurpleConversation *)convs->data;

        if (PIDGIN_IS_PIDGIN_CONVERSATION(conv)) {
            update_alert(PIDGIN_CONVERSATION(conv));
        }

        convs = convs->next;
    }
}

gboolean check_forum(PurpleAccount *account, char **who, char **message, PurpleConversation *conv, int *flags)
{
    UNUSED(account);
    UNUSED(who);
    UNUSED(flags);

    if (message == NULL || *message == NULL) {
        return FALSE;
    }

    PidginConversation *gtkconv = PIDGIN_IS_PIDGIN_CONVERSATION(conv) ? PIDGIN_CONVERSATION(conv) : NULL;

    if (gtkconv) {
        gchar *jid = g_strdup(gtkconv->active_conv->name);

        // Strip resource part and convert to lowercase
        for (int i = 0; jid[i]; ++i) {
            if (jid[i] == '/') {
                jid[i] = '\0';
                break;
            }
            else {
                jid[i] = g_ascii_tolower(jid[i]);
            }
        }

        GtkIMHtml *label = GTK_IMHTML(g_object_get_data(G_OBJECT(gtkconv->lower_hbox), "verified_label"));
        int hasGoodUserMessage = GPOINTER_TO_INT(purple_conversation_get_data(conv, "verified-good-user"));

        if (!label && !hasGoodUserMessage) {
            // Check for good user
            for (GList *l = good_user_list; l != NULL; l = l->next) {
                GoodUser *gu = (GoodUser*)l->data;
                if (!g_strcmp0(jid, gu->jid)) {
                    purple_conversation_set_data(conv, "verified-good-user", GINT_TO_POINTER(1));
                    GString *msg = g_string_new("");
                    g_string_append_printf(msg, get_text(GOOD_USER), gu->login, gu->registered, gu->id);
                    purple_conversation_write(conv, "ver", msg->str, PURPLE_MESSAGE_RAW, time(NULL));
                    g_string_free(msg, TRUE);
                    break;
                }
            }
        }
    }

    for (GList *l = forum_list; l != NULL; l = l->next) {
        gchar *forum = (gchar*)l->data;
        if (purple_utf8_has_word(*message, forum)) {
            purple_conversation_write(conv, "ver", get_text(FORUM), PURPLE_MESSAGE_RAW, time(NULL));
            break;
        }
    }

    return FALSE;
}

void free_scammer(Scammer *sc)
{
    if (sc) {
        g_free(sc->jid);
        g_free(sc->url);
        sc->jid = NULL;
        sc->url = NULL;
    }
}

void free_good_user(GoodUser *gu)
{
    if (gu) {
        g_free(gu->jid);
        g_free(gu->login);
        g_free(gu->registered);
        gu->jid = NULL;
        gu->login = NULL;
        gu->registered = NULL;
    }
}

gboolean update_plugin()
{
    purple_util_fetch_url(UPDATE_URL, TRUE, "", TRUE, parse_plugin_update, NULL);

    return TRUE;
}

void update_alert(PidginConversation *conv)
{
    if (!conv->active_conv) {
        destroy_alert(conv);
    }

    gchar *jid = g_strdup(conv->active_conv->name);

    // Strip resource part and convert to lowercase
    for (int i = 0; jid[i]; ++i) {
        if (jid[i] == '/') {
            jid[i] = '\0';
            break;
        }
        else {
            jid[i] = g_ascii_tolower(jid[i]);
        }
    }

    gchar *name = g_strdup(conv->active_conv->title);

    // Convert to lowercase
    for (int i = 0; name[i]; ++i) {
        name[i] = g_ascii_tolower(name[i]);
    }

    gboolean nonAscii = !g_str_is_ascii(jid);

    gboolean likeUsername = FALSE;

    gchar *it = name;
    gchar *cur = it;
    gboolean has_dog = FALSE; // has '@' symbol
    gboolean has_point = FALSE;  // has '.' symbol

    // Check for like username
    while (TRUE) {
        if (*cur != '\0' && *cur != ' ') {
            if (*cur == '@') {
                has_dog = TRUE;
            }
            else if (*cur == '.' && has_dog) {
                has_point = TRUE;
            }
            cur++;
            continue;
        }

        if (*cur) {
            *cur = '\0';
            cur++;
        }

        if (has_point && g_strcmp0(it, jid)) {
            likeUsername = TRUE;
            break;
        }
        has_dog = FALSE;
        has_point = FALSE;
        it = cur;
        if (!*it) {
            break;
        }
    }

    GString *scammer_urls = NULL;

    // Check for scammer
    for (GList *l = scammer_list; l != NULL; l = l->next) {
        Scammer *scammer = (Scammer*)l->data;
        if (!g_strcmp0(jid, scammer->jid)) {
            if (!scammer_urls) {
                scammer_urls = g_string_new("");
            }

            scammer_urls = g_string_append(scammer_urls, "<br> ");
            scammer_urls = g_string_append(scammer_urls, scammer->url);
            scammer_urls = g_string_append(scammer_urls, " (");
            scammer_urls = g_string_append(scammer_urls, get_text(VERIFIED_FORUM));
            scammer_urls = g_string_append(scammer_urls, ")");
        }
    }

    g_free(jid);
    g_free(name);

    GString *msg = NULL;
    if (nonAscii) {
        msg = g_string_new("");
        msg = g_string_append(msg, get_text(NONASCII));
    }


    if (likeUsername) {
        msg = msg ? g_string_append(msg, "<br>") : g_string_new("");
        msg = g_string_append(msg, get_text(LIKE_USERNAME));
    }

    if (scammer_urls) {
        msg = msg ? g_string_append(msg, "<br>") : g_string_new("");
        msg = g_string_append(msg, get_text(SCAMMER));
        msg = g_string_append(msg, scammer_urls->str);
    }

    GtkIMHtml *label = GTK_IMHTML(g_object_get_data(G_OBJECT(conv->lower_hbox), "verified_label"));

    if (label && !msg) {
        destroy_alert(conv);
        label = NULL;
    }
    else if (!label && msg) {
        label = GTK_IMHTML(gtk_imhtml_new(NULL, NULL));
        gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(label), GTK_WRAP_WORD);

        GtkScrolledWindow *sw = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
        gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
        gtk_container_add(GTK_CONTAINER(sw), GTK_WIDGET(label));

        GtkHBox *lower_hbox = GTK_HBOX(gtk_widget_get_parent(conv->lower_hbox));
        gtk_box_pack_start(GTK_BOX(lower_hbox), GTK_WIDGET(sw), FALSE, FALSE, 0);

        GList *list = gtk_container_get_children(GTK_CONTAINER(lower_hbox));
        gint pos = g_list_index(list, conv->lower_hbox);
        if (pos >= 0) {
            gtk_box_reorder_child(GTK_BOX(lower_hbox), GTK_WIDGET(sw), pos);
        }

        gtk_widget_show_all(GTK_WIDGET(sw));

        g_object_set_data(G_OBJECT(conv->lower_hbox), "verified_label", label);
    }

    if (label) {
        gtk_imhtml_clear(label);
        gtk_imhtml_font_grow(label);
        gtk_imhtml_toggle_background(label, "#fd9f9a"); // Pink
        gtk_imhtml_append_text_with_images(label, msg->str, GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_SCROLL, NULL);
        g_string_free(msg, TRUE);
    }
}

void destroy_alert(PidginConversation *conv)
{
    GtkWidget *alert = g_object_get_data(G_OBJECT(conv->lower_hbox), "verified_label");
    if (alert != NULL) {
        GtkWidget *parent = gtk_widget_get_parent(alert);
        gtk_widget_destroy(parent);
        g_object_set_data(G_OBJECT(conv->lower_hbox), "verified_label", NULL);
    }
}

void update_conversation(PurpleConversation *conv, PurpleConvUpdateType type)
{
    PidginConversation *gtkconv = PIDGIN_CONVERSATION(conv);
    if (gtkconv && type == PURPLE_CONV_UPDATE_TITLE) {
        update_alert(gtkconv);
    }
}

void parse_plugin_update(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message)
{
    UNUSED(url_data);
    UNUSED(user_data);
    UNUSED(error_message);

    /* Check for null-terminated string */
    if (!url_text) {
        return;
    }

    GKeyFile *key_file = g_key_file_new();

    if (g_key_file_load_from_data(key_file, url_text, len, G_KEY_FILE_NONE, NULL)) {
        int skip_version = purple_prefs_get_int("/plugins/gtk/verified/skip-version");
        int version = g_key_file_get_integer(key_file, INI_GROUP, "version", NULL);
        if (version > VERSION && version > skip_version) {
            gchar *url = g_key_file_get_string(key_file, INI_GROUP, "url", NULL);
            if (url && strlen(url)) {
                switch (show_ask_update(version)) {
                case 3:
                    purple_util_fetch_url(url, TRUE, "", TRUE, install_update, NULL);
                    break;

                case 1:
                    purple_prefs_set_int("/plugins/gtk/verified/skip-version", version);
                    break;

                default:
                    break;
                }
            }

            g_free(url);
        }
    }

    g_key_file_free(key_file);
}

void install_update(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message)
{
    UNUSED(url_data);
    UNUSED(user_data);
    UNUSED(error_message);

    // error
    if (!url_text) {
        return;
    }

    GString *old_path = g_string_new(verified_plugin->path);
    old_path = g_string_append(old_path, ".old");

    g_remove(old_path->str);

    GString *new_path = g_string_new(verified_plugin->path);
    new_path = g_string_append(new_path, ".new");

    if (g_file_set_contents(new_path->str, url_text, len, NULL)) {
#ifndef _WIN32
        g_chmod(new_path->str, 0755);
#endif
        if (!g_rename(verified_plugin->path, old_path->str) && !g_rename(new_path->str, verified_plugin->path)) {
            show_update_done();
        }
    }

    g_string_free(old_path, TRUE);
    g_string_free(new_path, TRUE);
}

int show_ask_update(int new_version)
{
    GtkLabel *label_title = GTK_LABEL(gtk_label_new(get_text(NEW_VERSION_TITLE)));
    gtk_label_set_use_markup(label_title, TRUE);
    gtk_label_set_line_wrap_mode(label_title, FALSE);

    GString *string = g_string_new("");
    g_string_printf(string, get_text(NEW_VERSION_BODY), new_version, VERSION);
    GtkIMHtml *label = GTK_IMHTML(gtk_imhtml_new(NULL, NULL));
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(label), GTK_WRAP_NONE);
    gtk_imhtml_append_text_with_images(label, string->str, GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_SCROLL, NULL);
    g_string_free(string, TRUE);

    GtkVBox *vbox = GTK_VBOX(gtk_vbox_new(FALSE, 10));
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(label_title), FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(label), FALSE, FALSE, 0);

    GtkDialog *dlg = GTK_DIALOG(gtk_dialog_new());
    gtk_window_set_title(GTK_WINDOW(dlg), get_text(PLUGIN_UPDATE));
    gtk_container_set_border_width(GTK_CONTAINER(dlg), PIDGIN_HIG_BORDER);
    gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
    gtk_dialog_add_button(dlg, get_text(SKIP_VERSION), 1);
    gtk_dialog_add_button(dlg, get_text(REMIND_LATER), 2);
    gtk_dialog_add_button(dlg, get_text(INSTALL_UPDATE), 3);

    GtkVBox *content_area = GTK_VBOX(gtk_dialog_get_content_area(dlg));
    gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(vbox), TRUE, TRUE, 0);

    pidgin_auto_parent_window(GTK_WIDGET(dlg));
    gtk_widget_show_all(GTK_WIDGET(dlg));
    int result = gtk_dialog_run(GTK_DIALOG(dlg));
    gtk_widget_destroy(GTK_WIDGET(dlg));
    return result;
}

void show_update_done()
{
    GtkLabel *label = GTK_LABEL(gtk_label_new(get_text(NEED_RESTART)));
    gtk_label_set_line_wrap_mode(label, FALSE);

    GtkAlignment *alignment = GTK_ALIGNMENT(gtk_alignment_new(0., 0., 0., 0.));
    gtk_alignment_set_padding(alignment, 10, 10, 50, 50);
    gtk_container_add(GTK_CONTAINER(alignment), GTK_WIDGET(label));

    GtkDialog *dlg = GTK_DIALOG(gtk_dialog_new());
    gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
    gtk_dialog_add_button(dlg, GTK_STOCK_CLOSE, 1);
    gtk_container_set_border_width(GTK_CONTAINER(dlg), PIDGIN_HIG_BORDER);
    gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
    gtk_window_set_title(GTK_WINDOW(dlg), get_text(PLUGIN_UPDATE));

    GtkVBox *content_area = GTK_VBOX(gtk_dialog_get_content_area(dlg));
    gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(alignment), TRUE, TRUE, 0);

    pidgin_auto_parent_window(GTK_WIDGET(dlg));
    gtk_widget_show_all(GTK_WIDGET(dlg));
    gtk_dialog_run(GTK_DIALOG(dlg));
    gtk_widget_destroy(GTK_WIDGET(dlg));
}

void show_self_scammer(gchar *blacks)
{
    GtkIMHtml *label = GTK_IMHTML(gtk_imhtml_new(NULL, NULL));
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(label), GTK_WRAP_NONE);

    gtk_imhtml_append_text_with_images(label, blacks, GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_SCROLL, NULL);

    GtkDialog *dlg = GTK_DIALOG(gtk_dialog_new());
    gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
    gtk_dialog_add_button(dlg, GTK_STOCK_CLOSE, 1);
    gtk_window_set_title(GTK_WINDOW(dlg), get_text(PLUGIN_WARNING));

    GtkVBox *content_area = GTK_VBOX(gtk_dialog_get_content_area(dlg));
    gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(label), TRUE, TRUE, 0);

    pidgin_auto_parent_window(GTK_WIDGET(dlg));
    gtk_widget_show_all(GTK_WIDGET(dlg));
    gtk_dialog_run(GTK_DIALOG(dlg));
    gtk_widget_destroy(GTK_WIDGET(dlg));
}

void show_wrong_plugin_path(gchar *correct_path)
{
    GString *str = g_string_new("");
    g_string_printf(str, get_text(WRONG_PATH), verified_plugin->path, correct_path);

    GtkLabel *label = GTK_LABEL(gtk_label_new(str->str));
    gtk_label_set_line_wrap_mode(label, FALSE);

    g_string_free(str, TRUE);

    GtkDialog *dlg = GTK_DIALOG(gtk_dialog_new());
    gtk_container_set_border_width(GTK_CONTAINER(dlg), PIDGIN_HIG_BORDER);
    gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
    gtk_dialog_add_button(dlg, GTK_STOCK_CLOSE, 1);
    gtk_window_set_title(GTK_WINDOW(dlg), get_text(PLUGIN_ERROR));

    GtkVBox *content_area = GTK_VBOX(gtk_dialog_get_content_area(dlg));
    gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(label), TRUE, TRUE, 0);

    pidgin_auto_parent_window(GTK_WIDGET(dlg));
    gtk_widget_show_all(GTK_WIDGET(dlg));
    gtk_dialog_run(GTK_DIALOG(dlg));
    gtk_widget_destroy(GTK_WIDGET(dlg));
}

gboolean check_news()
{
    purple_util_fetch_url(NEWS_URL, TRUE, "", TRUE, parse_news, NULL);

    return TRUE;
}

void parse_news(PurpleUtilFetchUrlData *url_data, gpointer user_data, const gchar *url_text, gsize len, const gchar *error_message)
{
    UNUSED(url_data);
    UNUSED(user_data);
    UNUSED(error_message);
    UNUSED(len);

    // Error
    if (!url_text) {
        return;
    }

    size_t i = 0;
    json_object *jobj = json_tokener_parse(url_text);

    if (!jobj) {
        return;
    }

#if GLIB_CHECK_VERSION(2, 62, 0)
    GTimeZone *tz = g_time_zone_new_utc();
    GDateTime *now = g_date_time_new_now_utc();
#else
    time_t now;
    struct tm *tmt;
    time(&now);
#endif

    GString *news = g_string_new("");
    GString *hashes = g_string_new("");
    GString *hash = g_string_new("");
    GString *prev_hashes = g_string_new(purple_prefs_get_string("/plugins/gtk/verified/news-hashes"));

    for (i = 0; i < json_object_array_length(jobj); ++i) {
        json_object *job = json_object_array_get_idx(jobj, i);
        json_object *message_ob;
        json_object *expires_ob;

        json_object_object_get_ex(job, "message", &message_ob);
        json_object_object_get_ex(job, "expires", &expires_ob);

        const char *message = json_object_get_string(message_ob);
        g_string_printf(hash, "(%ud)", g_str_hash(message));
        g_string_append(hashes, hash->str);

        const char *expires = json_object_get_string(expires_ob);
#if GLIB_CHECK_VERSION(2, 62, 0)
        GDateTime *dt = NULL;
#else
        GTimeVal dt;
        dt.tv_sec = 0;
        dt.tv_usec = 0;
#endif

        if (expires && strlen(expires) > 0) {
#if GLIB_CHECK_VERSION(2, 62, 0)
            dt = g_date_time_new_from_iso8601(expires, tz);
#else
            char *expiresIso = g_strdup_printf("%sZ", expires);
            char *space = strstr(expiresIso, " ");
            if (space) {
                *space = 'T';
            }
            g_time_val_from_iso8601(expiresIso, &dt);
            g_free(expiresIso);
#endif
        }

#if GLIB_CHECK_VERSION(2, 62, 0)
        if (strstr(prev_hashes->str, hash->str) || !message || strlen(message) < 1 || (dt && g_date_time_compare(dt, now) < 0)) {
#else
        if (strstr(prev_hashes->str, hash->str) || !message || strlen(message) < 1 || (dt.tv_sec && dt.tv_sec < now)) {
#endif
            continue;
        }

        news = g_string_append(news, message);
        news = g_string_append(news, "<hr>");
    }

    json_object_put(jobj);

    purple_prefs_set_string("/plugins/gtk/verified/news-hashes", hashes->str);

    if (news->len > 0) {
        GtkIMHtml *label = GTK_IMHTML(gtk_imhtml_new(NULL, NULL));
        gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(label), GTK_WRAP_NONE);
        gtk_imhtml_append_text_with_images(label, news->str, GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_SCROLL, NULL);

        GtkDialog *dlg = GTK_DIALOG(gtk_dialog_new());
        gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
        gtk_dialog_add_button(dlg, GTK_STOCK_CLOSE, 1);
        gtk_window_set_title(GTK_WINDOW(dlg), get_text(NEWS_TITLE));

        GtkVBox *content_area = GTK_VBOX(gtk_dialog_get_content_area(dlg));
        gtk_box_pack_start(GTK_BOX(content_area), GTK_WIDGET(label), TRUE, TRUE, 0);

        pidgin_auto_parent_window(GTK_WIDGET(dlg));
        gtk_widget_show_all(GTK_WIDGET(dlg));

        gtk_dialog_run(GTK_DIALOG(dlg));
        gtk_widget_destroy(GTK_WIDGET(dlg));
    }

#if GLIB_CHECK_VERSION(2, 62, 0)
    g_time_zone_unref(tz);
    g_date_time_unref(now);
#endif
    g_string_free(news, TRUE);
    g_string_free(hashes, TRUE);
    g_string_free(hash, TRUE);
    g_string_free(prev_hashes, TRUE);
}

PURPLE_INIT_PLUGIN (verified, init_plugin, info)
