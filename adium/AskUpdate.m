#import "AskUpdate.h"

#import <Adium/AISharedAdium.h>
#import <Adium/AIAdiumProtocol.h>
#import <Adium/AIPreferenceControllerProtocol.h>
#import <Adium/AIHTMLDecoder.h>

#import <AIUtilities/AIStringUtilities.h>

@interface AskUpdate ()
{
    int _newVersion;
    int _currentVersion;
    NSURL *_newVersionUrl;
    NSMutableData *_data;
}
@end

@implementation AskUpdate

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    [[self window] setTitle:AILocalizedString(@"PLUGIN_UPDATE", nil)];

    NSView *superview = self.remindLaterButton.superview;
    NSScrollView *headScrollView = self.headScrollView;
    NSTextView *headTextView = headScrollView.documentView;
    NSScrollView *bodyScrollView = self.bodyScrollView;
    NSTextView *bodyTextView = bodyScrollView.documentView;
    NSButton *skipButton = self.skipButton;
    NSButton *remindLaterButton = self.remindLaterButton;
    NSButton *installButton = self.installButton;

    headScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    bodyScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    bodyTextView.translatesAutoresizingMaskIntoConstraints = NO;
    skipButton.translatesAutoresizingMaskIntoConstraints = NO;
    remindLaterButton.translatesAutoresizingMaskIntoConstraints = NO;
    installButton.translatesAutoresizingMaskIntoConstraints = NO;

    headTextView.editable = NO;
    bodyTextView.editable = NO;

    [headTextView.textStorage setAttributedString:[AIHTMLDecoder decodeHTML:AILocalizedString(@"NEW_VERSION_TITLE", nil)]];
    
    NSString *str = [NSString stringWithFormat:AILocalizedString(@"NEW_VERSION_BODY", nil), _newVersion, _currentVersion];
    
    [bodyTextView.textStorage setAttributedString:[AIHTMLDecoder decodeHTML:str]];
    
    skipButton.title = AILocalizedString(@"SKIP_VERSION", nil);

    remindLaterButton.title = AILocalizedString(@"REMIND_LATER", nil);

    installButton.title = AILocalizedString(@"INSTALL_UPDATE", nil);

    // headTextView constraints
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:headScrollView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.
                                                           constant:18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:headScrollView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.
                                                           constant:0]];
    
    [headTextView setMaxSize:NSMakeSize(FLT_MAX, FLT_MAX)];
    [headTextView setHorizontallyResizable:YES];
    [[headTextView textContainer] setWidthTracksTextView:NO];
    [[headTextView textContainer] setContainerSize:NSMakeSize(FLT_MAX, FLT_MAX)];

    [headTextView.layoutManager ensureLayoutForTextContainer:headTextView.textContainer];
    NSSize size = [headTextView.layoutManager usedRectForTextContainer:headTextView.textContainer].size;

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:headScrollView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:size.width]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:headScrollView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:size.height]];
    
    // bodyScrollView constraints
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:bodyScrollView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:headTextView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:bodyScrollView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.
                                                           constant:18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:bodyScrollView
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.
                                                           constant:-18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:bodyScrollView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:remindLaterButton
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.
                                                           constant:-18.]];

    [bodyTextView setMaxSize:NSMakeSize(FLT_MAX, FLT_MAX)];
    [bodyTextView setHorizontallyResizable:YES];
    [[bodyTextView textContainer] setWidthTracksTextView:NO];
    [[bodyTextView textContainer] setContainerSize:NSMakeSize(FLT_MAX, FLT_MAX)];

    [bodyTextView.layoutManager ensureLayoutForTextContainer:bodyTextView.textContainer];
    size = [bodyTextView.layoutManager usedRectForTextContainer:bodyTextView.textContainer].size;

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:bodyScrollView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:size.width]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:bodyScrollView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:size.height]];

    // skipButton constraints
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:skipButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:bodyScrollView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:skipButton
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:-18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:skipButton
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:remindLaterButton
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.
                                                           constant:-18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:skipButton
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.
                                                           constant:18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:skipButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:skipButton.intrinsicContentSize.width]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:skipButton
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:skipButton.intrinsicContentSize.height]];

    // remindLaterButton constraints
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:remindLaterButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:bodyScrollView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:remindLaterButton
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:-18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:remindLaterButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.
                                                           constant:0]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:remindLaterButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:remindLaterButton.intrinsicContentSize.width]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:remindLaterButton
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:remindLaterButton.intrinsicContentSize.height]];

    // installButton constraints
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:installButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:bodyScrollView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:installButton
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:-18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:installButton
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:remindLaterButton
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.
                                                           constant:18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:installButton
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationLessThanOrEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.
                                                           constant:-18]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:installButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:installButton.intrinsicContentSize.width]];

    [superview addConstraint:[NSLayoutConstraint constraintWithItem:installButton
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.
                                                           constant:installButton.intrinsicContentSize.height]];
}

- (IBAction)skipVersion:(id)sender
{
    [[adium preferenceController] setPreference:[NSNumber numberWithInt:_newVersion] forKey:@"SkipVersion" group:@"VerifiedPlugin"];
    [self close];
}

- (IBAction)remindLater:(id)sender
{
    [self close];
}

- (IBAction)installVersion:(id)sender
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:_newVersionUrl];
    [NSURLConnection connectionWithRequest:urlRequest delegate:self];
    [self close];
}

- (void)setNewVersion:(int)newVersion currentVersion:(int)currentVersion newVersionUrl:(nonnull NSURL *)newVersionUrl
{
    _newVersion = newVersion;
    _currentVersion = currentVersion;
    _newVersionUrl = newVersionUrl;
}

- (void)connection:(nonnull NSURLConnection *)connection didReceiveResponse:(nonnull NSURLResponse *)response
{
    _data = [[NSMutableData alloc] init];
}

- (void)connection:(nonnull NSURLConnection *)connection didReceiveData:(nonnull NSData *)data
{
    [_data appendData:data];
}

- (nonnull NSCachedURLResponse *)connection:(nonnull NSURLConnection *)connection willCacheResponse:(nonnull NSCachedURLResponse *)cachedResponse
{
    return nil;
}

- (NSString *)UUIDString
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (NSString *)CFBridgingRelease(string);
}

- (void)connectionDidFinishLoading:(nonnull NSURLConnection *)connection
{
    NSString *basePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"verifiedplugin-%@", [self UUIDString]]];
    [[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *tempFilePath = [basePath stringByAppendingPathComponent:@"verifiedplugin.zip"];
    if (![_data writeToFile:tempFilePath atomically:NO]) {
        return;
    }
    
    NSTask *unzip = [[NSTask alloc] init];
    [unzip setLaunchPath:@"/usr/bin/unzip"];
    [unzip setArguments:[NSArray arrayWithObjects:@"-u", @"-d", basePath, tempFilePath, nil]];
    [unzip launch];
    [unzip waitUntilExit];
    if (unzip.terminationStatus) {
        return;
    }

    NSTask *open = [[NSTask alloc] init];
    [open setLaunchPath:@"/usr/bin/open"];
    [open setArguments:[NSArray arrayWithObject:[NSString stringWithFormat:@"%@/VerifiedPlugin.AdiumPlugin", basePath]]];
    [open launch];
    [open waitUntilExit];
}  

// Allow wrong certificate https://stackoverflow.com/questions/933331/how-to-use-nsurlconnection-to-connect-with-ssl-for-an-untrusted-cert/2033823#2033823
- (BOOL)connection:(nonnull NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(nonnull NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(nonnull NSURLConnection *)connection didReceiveAuthenticationChallenge:(nonnull NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    }
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

@end
