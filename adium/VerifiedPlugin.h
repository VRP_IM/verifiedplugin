#import "NewsWindow.h"

#import <Foundation/Foundation.h>

#import <Cocoa/Cocoa.h>

#import <Adium/AIPlugin.h>
#import <Adium/AIContentControllerProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@class SelfScammerWindow;
@class AskUpdate;
@class Scammer;
@class GoodUser;

@interface VerifiedPlugin : AIPlugin<AIContentFilter, NSURLConnectionDelegate>
{
    NSMutableData *_forumData;
    NSMutableData *_scammerData;
    NSMutableData *_pluginData;
    NSMutableData *_newsData;
    NSMutableData *_goodUserData;
    NSURLConnection *_forumConnection;
    NSURLConnection *_scammerConnection;
    NSURLConnection *_pluginConnection;
    NSURLConnection *_newsConnection;
    NSURLConnection *_goodUserConnection;
    NSMutableArray<NSString *> *_forumArray;
    NSMutableArray<Scammer *> *_scammerArray;
    NSMutableArray<GoodUser *> *_goodUserArray;
    SelfScammerWindow *_selfScammerWindow;
    NewsWindow *_newsWindow;
    AskUpdate *_askUpdate;
}

- (void)updateDb:(nullable NSTimer *)timer;

@end

@interface Scammer : NSObject
{
    NSString *_jid;
    NSString *_url;
}

@property (copy, nonatomic) NSString *jid;
@property (copy, nonatomic) NSString *url;

- (Scammer *) init;

- (Scammer *) initJid:(nullable NSString *)jid url:(nullable NSString *)url;

- (NSString *) description;

@end

@interface GoodUser : NSObject
{
    NSString *_jid;
    NSString *_registered;
    NSString *_login;
    int _userId;
}

@property (copy, nonatomic) NSString *jid;
@property (copy, nonatomic) NSString *registered;
@property (copy, nonatomic) NSString *login;
@property (nonatomic) int userId;

- (GoodUser *) init;

- (GoodUser *) initJid:(nullable NSString *)jid registered:(nullable NSString *)registered login:(nullable NSString *)login userId:(int) userId;

@end

NS_ASSUME_NONNULL_END
