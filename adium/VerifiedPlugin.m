#import "VerifiedPlugin.h"

#import "SelfScammerWindow.h"
#import "AskUpdate.h"

#import <Adium/AIContentNotification.h>
#import <Adium/AIContentMessage.h>
#import <Adium/AIContentEvent.h>
#import <Adium/AIHTMLDecoder.h>
#import <Adium/AIChat.h>
#import <Adium/AIInterfaceControllerProtocol.h>
#import <Adium/AICorePluginLoader.h>
#import <Adium/AISharedAdium.h>
#import <Adium/AIListContact.h>
#import <Adium/AIChatControllerProtocol.h>
#import <Adium/AIAccountControllerProtocol.h>
#import <Adium/AIPreferenceControllerProtocol.h>

#import <AIUtilities/AIStringUtilities.h>

#define SELF_BL_INTERVAL (24 * 60 * 60) /* 1 day */
#define UPDATE_DB_INTERVAL (3 * 60 * 60) /* 3 hours */
#define UPDATE_PLUGIN_INTERVAL (24 * 60 * 60) /* 1 day */
#define NEWS_TIMER_INTERVAL (10 * 60) /* 10 minutes */
#define FORUM_DB_URL @"https://vrp.im/db/db_forum.json"
#define SCAMMER_DB_URL @"https://vrp.im/db/db.json"
#define GOOD_USER_DB_URL @"https://vrp.im/db/db_goodusers.json"
#define UPDATE_URL @"https://vrp.im/adium-verified-appcast.json"
#define NEWS_URL @"https://vrp.im/db/message.json"


static unsigned long hash(const unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

@class AIWebkitMessageViewStyle;
@class AIWebKitMessageViewPlugin;

@interface AIWebkitMessageViewStyle : NSObject
- (NSArray *)validSenderColors;

@property (weak, readwrite, nonatomic) NSColor *customBackgroundColor;
@property (readwrite, nonatomic) BOOL allowTextBackgrounds;

@end

@interface AIWebKitMessageViewPlugin : NSObject
- (AIWebkitMessageViewStyle *) currentMessageStyleForChat:(AIChat *)chat;
@end

@interface AICoreComponentLoader : NSObject <AIController>
- (id <AIPlugin>)pluginWithClassName:(NSString *)className;
@end

@implementation VerifiedPlugin

- (void)installPlugin
{
    // Changes actual message and non-message content
    [[adium contentController] registerContentFilter:self ofType:AIFilterMessageDisplay direction:AIFilterOutgoing];
    [[adium contentController] registerContentFilter:self ofType:AIFilterMessageDisplay direction:AIFilterIncoming];
    
    [NSTimer scheduledTimerWithTimeInterval:UPDATE_DB_INTERVAL
                                     target:self
                                   selector:@selector(updateDb:)
                                   userInfo:nil
                                    repeats:TRUE];

    [NSTimer scheduledTimerWithTimeInterval:UPDATE_PLUGIN_INTERVAL
                                     target:self
                                   selector:@selector(updatePlugin:)
                                   userInfo:nil
                                    repeats:TRUE];

    [NSTimer scheduledTimerWithTimeInterval:NEWS_TIMER_INTERVAL
                                     target:self
                                   selector:@selector(checkNews:)
                                   userInfo:nil
                                    repeats:TRUE];


    [self updateDb:nil];
    [self updatePlugin:nil];
    [self checkNews:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(chatDidOpen:)
                                                 name:Chat_DidOpen
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(chatDidOpen:)
                                                 name:Chat_AttributesChanged
                                               object:nil];
}

- (void)uninstallPlugin
{
}

- (BOOL)nonAsciiJid:(NSString *)jid
{
    NSMutableCharacterSet *cSet = [NSMutableCharacterSet characterSetWithRange:NSMakeRange(0, 127)];
    [cSet addCharactersInRange:NSMakeRange(0x20a0, 0x20bf)];
    
    return [jid rangeOfCharacterFromSet:[cSet invertedSet]].location != NSNotFound;
}

- (void)chatDidOpen:(NSNotification *)notification
{
    AIChat *chat = [notification object];
    [self updateAlertChat:chat];
}

- (NSColor*)colorWithHexColorString:(NSString*)inColorString
{
    NSColor *result = nil;
    unsigned colorCode = 0;
    unsigned char redByte, greenByte, blueByte;

    if (inColorString) {
         NSScanner *scanner = [NSScanner scannerWithString:inColorString];
         (void) [scanner scanHexInt:&colorCode]; // ignore error
    }
    redByte = (unsigned char)(colorCode >> 16);
    greenByte = (unsigned char)(colorCode >> 8);
    blueByte = (unsigned char)(colorCode); // masks off high bits

    result = [NSColor colorWithCalibratedRed:(CGFloat)redByte / 0xff
                                       green:(CGFloat)greenByte / 0xff
                                        blue:(CGFloat)blueByte / 0xff
                                       alpha:1.0];
    return result;
}

- (void)updateScammerWarningHeight:(NSView *)superview
{
    for (int i = 0; i < [superview.subviews count]; ++i) {
        if ([[[superview.subviews objectAtIndex:i] className] compare:@"NSTextView"] == NSOrderedSame) {
            NSTextView *textView = (NSTextView*)[superview.subviews objectAtIndex:i];
            NSArray<NSLayoutConstraint *> *constraints = [superview constraints];
            for (int j = 0; j < [constraints count]; ++j) {
                NSLayoutConstraint *constraint = [constraints objectAtIndex:j];
                if (constraint.firstItem == textView && constraint.firstAttribute == NSLayoutAttributeHeight) {
                    CGFloat textHeight = 0;
                    if ([[textView textStorage] length] != 0) {
                        [[textView layoutManager] glyphRangeForTextContainer:[textView textContainer]];
                        textHeight = [[textView layoutManager] usedRectForTextContainer:[textView textContainer]].size.height;
                        
                        constraint.constant = textHeight;
                    }
                }
            }
            break;
        }
    }
}

- (void)updateScammer:(NSNotification *)notif
{
    [self updateScammerWarningHeight:notif.object];
}

- (void)updateAlertChat:(AIChat *)conv
{
    NSString *jid = [conv listObject].formattedUID.lowercaseString;
    NSString *name = [conv listObject].ownPhoneticName.lowercaseString;
    NSView *messageView = conv.chatContainer.chatViewController.messagesScrollView;

    if (![jid length] || !messageView) {
        return;
    }

    BOOL nonAscii = [self nonAsciiJid:jid];

    BOOL likeUsername = NO;

    NSArray<NSString *> *nameArr = [name componentsSeparatedByString:@" "];
    for (int i = 0; i < nameArr.count; ++i) {
        NSString *part = [nameArr objectAtIndex:i];
        
        if ([part compare:jid] == NSOrderedSame) {
            continue;
        }
        
        NSRange range = [part rangeOfString:@"@"];
        if (range.location != NSNotFound) {
            range = [[part substringFromIndex:range.location] rangeOfString:@"."];
            if (range.location != NSNotFound) {
                likeUsername = YES;
                break;
            }
        }
    }

    NSMutableString *scammer_urls = [[NSMutableString alloc] init];

    for (int i = 0; i < _scammerArray.count; ++i) {
        Scammer *scammer = [_scammerArray objectAtIndex:i];
        
        if ([scammer.jid compare:jid] == NSOrderedSame) {
            [scammer_urls appendString:[NSString stringWithFormat:@"<br/> %@", scammer.url]];
        }
    }

    NSMutableString *msg = [[NSMutableString alloc] init];
    if (nonAscii) {
        [msg appendString:AILocalizedString(@"NONASCII", nil)];
    }


    if (likeUsername) {
        if (msg.length) {
            [msg appendString:@"<br/>"];
        }
        [msg appendString:AILocalizedString(@"LIKE_USERNAME", nil)];
    }

    if (scammer_urls.length) {
        if (msg.length) {
            [msg appendString:@"<br/>"];
        }
        [msg appendString:AILocalizedString(@"SCAMMER", nil)];
        [msg appendString:scammer_urls];
    }
    
    NSView *superview = messageView.superview;
    NSTextView *textView = nil;
    
    // Find alert view
    for (int i = 0; i < superview.subviews.count; ++i) {
        if ([[[superview.subviews objectAtIndex:i] className] compare:@"NSTextView"] == NSOrderedSame) {
            textView = (NSTextView*)[superview.subviews objectAtIndex:i];
        }
    }

    if (!msg.length && textView) {
        [textView removeFromSuperview];
        messageView.translatesAutoresizingMaskIntoConstraints = YES;
        [messageView setFrame:superview.bounds];
    }
    else if (msg.length && !textView) {
        messageView.translatesAutoresizingMaskIntoConstraints = NO;

        textView = [[NSTextView alloc] init];
        [[textView textStorage] setAttributedString:[AIHTMLDecoder decodeHTML:msg]];
        textView.backgroundColor = [self colorWithHexColorString:@"fd9f9a"];
        textView.translatesAutoresizingMaskIntoConstraints = NO;

        // Make URL's clickable
        textView.automaticLinkDetectionEnabled = YES;
        textView.editable = YES;
        [textView checkTextInDocument:nil];
        textView.editable = NO;

        [superview addSubview:textView];
        
        // messageView constraints
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:messageView
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:superview
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.
                                                               constant:0.]];
        
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:messageView
                                                              attribute:NSLayoutAttributeLeft
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:superview
                                                              attribute:NSLayoutAttributeLeft
                                                             multiplier:1.
                                                               constant:0]];
        
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:messageView
                                                              attribute:NSLayoutAttributeRight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:superview
                                                              attribute:NSLayoutAttributeRight
                                                             multiplier:1.
                                                               constant:0]];
        
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:messageView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:textView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.
                                                               constant:0]];
        // textView constraints

        [superview addConstraint:[NSLayoutConstraint constraintWithItem:textView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:superview
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.
                                                               constant:0.]];
        
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:textView
                                                              attribute:NSLayoutAttributeLeft
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:superview
                                                              attribute:NSLayoutAttributeLeft
                                                             multiplier:1.
                                                               constant:0]];
        
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:textView
                                                              attribute:NSLayoutAttributeRight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:superview
                                                              attribute:NSLayoutAttributeRight
                                                             multiplier:1.
                                                               constant:0]];
        
        [superview addConstraint:[NSLayoutConstraint constraintWithItem:textView
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.
                                                               constant:0]];
        
        // Constraint is not apply immediately. Need to set correct initial width for correct initial height calculation.
        [textView setFrameSize:NSMakeSize(messageView.frame.size.width, 0.)];
        [self updateScammerWarningHeight:superview];
        
        superview.postsFrameChangedNotifications = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateScammer:)
                                                     name:@"NSViewFrameDidChangeNotification"
                                                   object:superview];

    }
    else if (msg.length && textView) {
        [[textView textStorage] setAttributedString:[AIHTMLDecoder decodeHTML:msg]];
        [self updateScammerWarningHeight:superview];
    }
}

- (NSAttributedString *)filterAttributedString:(NSAttributedString *)inAttributedString context:(id)context
{
    AIContentMessage *origMsg = (AIContentMessage*)context;
    
    if (origMsg) {
        NSString *jid = [origMsg.chat listObject].formattedUID.lowercaseString;
        NSString *name = [origMsg.chat listObject].ownPhoneticName.lowercaseString;
        BOOL isScammer = [self nonAsciiJid:jid];

        NSArray<NSString *> *nameArr = [name componentsSeparatedByString:@" "];
        for (int i = 0; i < nameArr.count; ++i) {
            NSString *part = [nameArr objectAtIndex:i];
            
            if ([part compare:jid] == NSOrderedSame) {
                continue;
            }
            
            NSRange range = [part rangeOfString:@"@"];
            if (range.location != NSNotFound) {
                range = [[part substringFromIndex:range.location] rangeOfString:@"."];
                if (range.location != NSNotFound) {
                    isScammer = YES;
                    break;
                }
            }
        }
        
        for (int i = 0; i < _scammerArray.count; ++i) {
            Scammer *scammer = [_scammerArray objectAtIndex:i];
            
            if ([scammer.jid compare:jid] == NSOrderedSame) {
                isScammer = TRUE;
                break;
            }
        }
        
        if (!isScammer && ![origMsg.chat boolValueForProperty:@"verified-good-user"]) {
            for (GoodUser *gu in _goodUserArray) {
                if ([gu.jid compare:jid] == NSOrderedSame) {
                    [origMsg.chat setValue:(id)TRUE forProperty:@"verified-good-user" notify:NotifyNever];
                    NSString *str = [NSString stringWithFormat:AILocalizedString(@"GOOD_USER", nil), gu.login, gu.registered, gu.userId];
                    [[adium contentController] displayEvent:str
                                                     ofType:@"verified"
                                                     inChat:origMsg.chat];
                }
            }
        }
    
        for (NSString *url in _forumArray) {
            if ([[inAttributedString string] rangeOfString:url options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [[adium contentController] displayEvent:AILocalizedString(@"FORUM", nil)
                                                 ofType:@"verified"
                                                 inChat:origMsg.chat];
            }
        }
    }

    return inAttributedString;
}

- (CGFloat)filterPriority
{
    return DEFAULT_FILTER_PRIORITY;
}

- (void)updateDb:(nullable NSTimer *)timer
{
    NSURL *url = [NSURL URLWithString:FORUM_DB_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    _forumConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];

    url = [NSURL URLWithString:SCAMMER_DB_URL];
    urlRequest = [NSURLRequest requestWithURL:url];
    _scammerConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
    
    url = [NSURL URLWithString:GOOD_USER_DB_URL];
    urlRequest = [NSURLRequest requestWithURL:url];
    _goodUserConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)updatePlugin:(nullable NSTimer *)timer
{
    NSURL *url = [NSURL URLWithString:UPDATE_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    _pluginConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)checkNews:(nullable NSTimer *)timer
{
    NSURL *url = [NSURL URLWithString:NEWS_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    _newsConnection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
 {
     if (connection == _forumConnection) {
         _forumData = [[NSMutableData alloc] init];
     }
     else if (connection == _scammerConnection) {
         _scammerData = [[NSMutableData alloc] init];
     }
     else if (connection == _pluginConnection) {
         _pluginData = [[NSMutableData alloc] init];
     }
     else if (connection == _newsConnection) {
         _newsData = [[NSMutableData alloc] init];
     }
     else if (connection == _goodUserConnection) {
         _goodUserData = [[NSMutableData alloc] init];
     }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
     if (connection == _forumConnection) {
         [_forumData appendData:data];
     }
     else if (connection == _scammerConnection) {
         [_scammerData appendData:data];
     }
     else if (connection == _pluginConnection) {
         [_pluginData appendData:data];
     }
     else if (connection == _newsConnection) {
         [_newsData appendData:data];
     }
     else if (connection == _goodUserConnection) {
         [_goodUserData appendData:data];
     }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse*)cachedResponse
{
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == _forumConnection) {
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:_forumData options:NO error:nil];
        if (arr && [arr isKindOfClass:[NSArray class]]) {
            for (int i = 0; i < [arr count]; ++i) {
                NSDictionary *dict = [arr objectAtIndex:i];
                if (!dict) {
                    continue;
                }
                NSString *str = [dict valueForKey:@"type"];
                if (!str || ![str isKindOfClass:[NSString class]] || [str compare:@"table"] != NSOrderedSame) {
                    continue;
                }
                
                _forumArray = [[NSMutableArray alloc] init];
                NSArray *dataArr = [dict valueForKey:@"data"];
                for (int j = 0; j < [dataArr count]; ++j) {
                    NSString *str = [[dataArr objectAtIndex:j] valueForKey:@"l"];
                    if ([str length]) {
                        [_forumArray addObject:str.lowercaseString];
                    }
                }
                break;
            }
        }
        _forumData = nil;
    }
    else if (connection == _scammerConnection) {
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:_scammerData options:NO error:nil];
        if (arr && [arr isKindOfClass:[NSArray class]]) {
            for (int i = 0; i < [arr count]; ++i) {
                NSDictionary *dict = [arr objectAtIndex:i];
                if (!dict) {
                    continue;
                }
                NSString *str = [dict valueForKey:@"type"];
                if (!str || ![str isKindOfClass:[NSString class]] || [str compare:@"table"] != NSOrderedSame) {
                    continue;
                }
                
                _scammerArray = [[NSMutableArray alloc] init];
                NSArray *dataArr = [dict valueForKey:@"data"];
                for (int j = 0; j < [dataArr count]; ++j) {
                    NSString *jid = [[dataArr objectAtIndex:j] valueForKey:@"j"];
                    NSString *thread = [[dataArr objectAtIndex:j] valueForKey:@"t"];
                    if ([jid length] && [thread length]) {
                        Scammer *scammer = [[Scammer alloc] initJid:jid.lowercaseString
                                                                url:[NSString stringWithFormat:@"<span style=\"color: #0000ff\">/showthread.php?t=%@</span> (%@)", thread, AILocalizedString(@"VERIFIED_FORUM", nil)]];
                        [_scammerArray addObject:scammer];
                    }
                }
                
                NSEnumerator *enumerator = [[adium chatController].openChats objectEnumerator];
                AIChat *chat;
                
                while ((chat = [enumerator nextObject])) {
                    [self updateAlertChat:chat];
                }

                // Collect all own jids
                NSMutableArray<NSString *> *ownJids = [[NSMutableArray alloc] init];
                NSArray<AIAccount *> *accounts = [[adium accountController] accounts];
                for (int i = 0; i < accounts.count; ++i) {
                    [ownJids addObject:[accounts objectAtIndex:i].explicitFormattedUID.lowercaseString];
                }

                // Find all blacks for all own jids
                NSMutableArray<NSString *> *selfBlackUrls = [[NSMutableArray alloc] init];
                for (int i = 0; i < ownJids.count; ++i) {
                    NSString *ownJid = [ownJids objectAtIndex:i];
                    for (int j = 0; j < _scammerArray.count; ++j) {
                        Scammer *scammer = [_scammerArray objectAtIndex:j];
                        if ([ownJid compare:scammer.jid] == NSOrderedSame) {
                            [selfBlackUrls addObject:scammer.url];
                        }
                    }

                }

                NSArray<NSString *> *oldSelfBlackUrls = [[adium preferenceController] preferenceForKey:@"SelfBlacks" group:@"VerifiedPlugin"];

                int counter = [[[adium preferenceController] preferenceForKey:@"SelfBlacksCounter" group:@"VerifiedPlugin"] intValue];
                NSDate *date = [[adium preferenceController] preferenceForKey:@"SelfBlacksDate" group:@"VerifiedPlugin"];

                if ([[NSDate date] timeIntervalSinceDate:date] < SELF_BL_INTERVAL) {
                    return;
                }


                NSMutableArray<NSString *> *copySelfBlackUrls = [NSMutableArray arrayWithArray:selfBlackUrls];
                
                for (int i = 0; i < oldSelfBlackUrls.count; ++i) {
                    [copySelfBlackUrls removeObject:[oldSelfBlackUrls objectAtIndex:i]];
                }
                
                if (selfBlackUrls.count && (copySelfBlackUrls.count || (counter < 2 && [[NSDate date] timeIntervalSinceDate:date] > SELF_BL_INTERVAL))) {
                    if (!_selfScammerWindow) {
                        _selfScammerWindow = [[SelfScammerWindow alloc] initWithWindowNibName:@"SelfScammerWindow"];
                        [_selfScammerWindow window];
                    }
                    
                    if (copySelfBlackUrls.count) {
                        counter = 0;
                    }

                    [[adium preferenceController] setPreference:[NSNumber numberWithInt:++counter] forKey:@"SelfBlacksCounter" group:@"VerifiedPlugin"];
                    [_selfScammerWindow showWindow:nil];
                    [_selfScammerWindow setUrls:selfBlackUrls];
                }
                else if (!selfBlackUrls.count) {
                    [[_selfScammerWindow window] close];
                }

                break;
            }
        }
        _scammerData = nil;
    }
    else if (connection == _pluginConnection) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:_pluginData options:NO error:nil];
        if (dict) {
            NSURL *url = [NSURL URLWithString:[dict valueForKey:@"url"]];
            int version = [[dict valueForKey:@"version"] intValue];
            int currentVersion = [(NSString *)[[NSBundle bundleForClass:[self class]] objectForInfoDictionaryKey: @"CFBundleVersion"] intValue];
            int skipVersion = [[[adium preferenceController] preferenceForKey:@"SkipVersion" group:@"VerifiedPlugin"] intValue];

            if (version > currentVersion && url.absoluteString.length && version != skipVersion) {
                _askUpdate = [[AskUpdate alloc] initWithWindowNibName:@"AskUpdate"];
                [_askUpdate setNewVersion:version
                           currentVersion:currentVersion
                            newVersionUrl:url];

                [_askUpdate showWindow:nil];
            }
        }
        _pluginData = nil;
    }
    else if (connection == _newsConnection) {
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:_newsData options:NO error:nil];
        
        NSMutableArray<NSString *> *messages = [[NSMutableArray alloc] init];
        
        if (arr && [arr isKindOfClass:[NSArray class]]) {
            NSArray<NSString *> *prevHashes = [[adium preferenceController] preferenceForKey:@"NewsHashes" group:@"VerifiedPlugin"];
            NSMutableArray<NSString *> *hashes = [NSMutableArray array];
            NSError *error = nil;
            NSRegularExpression *rx = [NSRegularExpression regularExpressionWithPattern:@"(https?://[^\\s<>]+)"
                                                                                options:NSRegularExpressionCaseInsensitive error:&error];

            for (int i = 0; i < [arr count]; ++i) {
                NSDictionary *dict = [arr objectAtIndex:i];
                if (!dict) {
                    continue;
                }
                NSString *message = [dict valueForKey:@"message"];
                NSString *expires = [dict valueForKey:@"expires"];

                [hashes addObject:@(hash((const unsigned char*)message.UTF8String)).stringValue];
                NSDate *expiresDate = nil;

                if (expires) {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

                    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                    formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];

                    expiresDate = [formatter dateFromString:expires];
                }

                if ([prevHashes containsObject:[hashes lastObject]] || !message || ![message isKindOfClass:[NSString class]] || (expiresDate && [expiresDate compare:[NSDate date]] == NSOrderedAscending)) {
                    continue;
                }


                message = [rx stringByReplacingMatchesInString:message
                                                       options:0
                                                         range:NSMakeRange(0, [message length])
                                                  withTemplate:@"<a href=\"$1\">$1</a>"];
                [messages addObject:message];
            }
        
            [[adium preferenceController] setPreference:hashes forKey:@"NewsHashes" group:@"VerifiedPlugin"];
        }
        
        if (messages.count > 0) {
            if (!_newsWindow) {
                _newsWindow = [[NewsWindow alloc] initWithWindowNibName:@"NewsWindow"];
                [_newsWindow window];
            }
            
            [_newsWindow showWindow:nil];
            [_newsWindow setMessages:messages];
        }
        
        _newsData = nil;
    }
    else if (connection == _goodUserConnection) {
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:_goodUserData options:NO error:nil];
        if (arr && [arr isKindOfClass:[NSArray class]]) {
            for (int i = 0; i < [arr count]; ++i) {
                NSDictionary *dict = [arr objectAtIndex:i];
                if (!dict) {
                    continue;
                }
                NSString *str = [dict valueForKey:@"type"];
                if (!str || ![str isKindOfClass:[NSString class]] || [str compare:@"table"] != NSOrderedSame) {
                    continue;
                }
                
                _goodUserArray = [[NSMutableArray alloc] init];
                NSArray *dataArr = [dict valueForKey:@"data"];
                for (int j = 0; j < [dataArr count]; ++j) {
                    NSString *jid = [[dataArr objectAtIndex:j] valueForKey:@"j"];
                    NSString *registered = [[dataArr objectAtIndex:j] valueForKey:@"r"];
                    NSString *login = [[dataArr objectAtIndex:j] valueForKey:@"l"];
                    int userId = [[[dataArr objectAtIndex:j] valueForKey:@"i"] intValue];
                    
                    if ([jid length] && [registered length] && [login length] && userId > 0) {
                        GoodUser *goodUser = [[GoodUser alloc] initJid:jid.lowercaseString registered:registered login:login userId:userId];
                        [_goodUserArray addObject:goodUser];
                    }
                }
            }
        }
        _goodUserData = nil;
    }
}

// Allow wrong certificate https://stackoverflow.com/questions/933331/how-to-use-nsurlconnection-to-connect-with-ssl-for-an-untrusted-cert/2033823#2033823
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    }
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

@end

@implementation Scammer

- (Scammer *)init
{
    return [self initJid:nil url:nil];
}

- (Scammer *)initJid:(nullable NSString *)jid url:(nullable NSString *)url
{
    self = [super init];
    if (self) {
        self.jid = jid;
        self.url = url;
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ - %@", self.jid, self.url];
}

@end

@implementation GoodUser

- (GoodUser *)init
{
    return [self initJid:nil registered:nil login:nil userId:0];
}

- (GoodUser *)initJid:(nullable NSString *)jid registered:(nullable NSString *)registered login:(nullable NSString *)login userId:(int)userId
{
    self = [super init];
    if (self) {
        self.jid = jid;
        self.registered = registered;
        self.login = login;
        self.userId = userId;
    }
    return self;
}

@end
