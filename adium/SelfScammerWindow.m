#import "SelfScammerWindow.h"

#import <Adium/AIHTMLDecoder.h>
#import <Adium/AISharedAdium.h>
#import <Adium/AIAdiumProtocol.h>
#import <Adium/AIPreferenceControllerProtocol.h>

#import <AIUtilities/AIStringUtilities.h>

@interface SelfScammerWindow ()
{
    NSArray<NSString *> *_urls;
}
@end

@implementation SelfScammerWindow

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    NSScrollView *scrollView = self.scrollView;
    NSTextView *textView = scrollView.documentView;
    NSButton *okButton = self.okButton;
    NSView *superview = okButton.superview;

    okButton.translatesAutoresizingMaskIntoConstraints = NO;
    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.translatesAutoresizingMaskIntoConstraints = NO;

    textView.editable = NO;
    
    [[self window] setTitle:AILocalizedString(@"PLUGIN_WARNING", nil)];
    
    // scrollView constraints
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:scrollView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.
                                                           constant:18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:scrollView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.
                                                           constant:18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:scrollView
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.
                                                           constant:-18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:scrollView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:okButton
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.
                                                           constant:-18.]];

    _widthConstraint = [NSLayoutConstraint constraintWithItem:scrollView
                                                    attribute:NSLayoutAttributeWidth
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.
                                                     constant:100];
    [superview addConstraint:_widthConstraint];

    _heightConstraint = [NSLayoutConstraint constraintWithItem:scrollView
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.
                                                      constant:100];
    [superview addConstraint:_heightConstraint];

    // okButton
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:okButton
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.
                                                           constant:-18]];
    
    [superview addConstraint:[NSLayoutConstraint constraintWithItem:okButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superview
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.
                                                           constant:0]];
}

- (IBAction)closeWindow:(id)sender
{
    [self close];
}

- (void)setUrls:(NSArray<NSString *> *)urls
{
    _urls = urls;
    NSMutableString *string = [[NSMutableString alloc] initWithString:AILocalizedString(@"SELF_BLACK", nil)];
    for (int i = 0; i < urls.count; ++i) {
        NSString *url = [urls objectAtIndex:i];
        [string appendFormat:@"<br/> %@", url];
    }
    
    NSTextView *textView = self.scrollView.documentView;

    [textView.textStorage setAttributedString:[AIHTMLDecoder decodeHTML:string]];
    
    [textView setMaxSize:NSMakeSize(FLT_MAX, FLT_MAX)];
    [textView setHorizontallyResizable:YES];
    [[textView textContainer] setWidthTracksTextView:NO];
    [[textView textContainer] setContainerSize:CGSizeMake(FLT_MAX, FLT_MAX)];

    [textView.layoutManager ensureLayoutForTextContainer:textView.textContainer];
    NSSize size = [textView.layoutManager usedRectForTextContainer:textView.textContainer].size;
    
    _widthConstraint.constant = size.width;
    _heightConstraint.constant = size.height;
}

- (void)windowWillClose:(nonnull NSNotification *)notification
{
    [[adium preferenceController] setPreference:[NSDate date] forKey:@"SelfBlacksDate" group:@"VerifiedPlugin"];
    [[adium preferenceController] setPreference:_urls forKey:@"SelfBlacks" group:@"VerifiedPlugin"];
}

@end
