#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface AskUpdate : NSWindowController<NSURLConnectionDelegate>

@property (assign) IBOutlet NSScrollView *headScrollView;
@property (assign) IBOutlet NSScrollView *bodyScrollView;
@property (assign) IBOutlet NSButton *skipButton;
@property (assign) IBOutlet NSButton *remindLaterButton;
@property (assign) IBOutlet NSButton *installButton;

- (void)setNewVersion:(int)newVersion
       currentVersion:(int)currentVersion
        newVersionUrl:(NSURL *)newVersionUrl;

- (IBAction)skipVersion:(id)sender;
- (IBAction)remindLater:(id)sender;
- (IBAction)installVersion:(id)sender;

// implement NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse*)cachedResponse;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace;
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
@end

NS_ASSUME_NONNULL_END
