#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelfScammerWindow : NSWindowController
{
    NSLayoutConstraint *_widthConstraint;
    NSLayoutConstraint *_heightConstraint;
}

@property (assign) IBOutlet NSScrollView *scrollView;
@property (assign) IBOutlet NSButton *okButton;

- (IBAction)closeWindow:(id)sender;
- (void)windowDidLoad;
- (void)windowWillClose:(NSNotification *)notification;
- (void)setUrls:(NSArray<NSString *> *)urls;

@end

NS_ASSUME_NONNULL_END
