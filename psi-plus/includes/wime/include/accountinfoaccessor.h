#pragma once

#include <QtPlugin>

#define UN __attribute__ ((unused))

class AccountInfoAccessingHost;

class AccountInfoAccessor
{
public:
    virtual ~AccountInfoAccessor()
    {}

    virtual void setAccountInfoAccessingHost(AccountInfoAccessingHost *host) = 0;
    virtual void accountAdded(UN int account) {}
    virtual void accountRemoved(UN int account) {}
    virtual void accountCreated(UN int account) {}
    virtual void accountUpdated(UN int account) {}
};

#undef UN

Q_DECLARE_INTERFACE(AccountInfoAccessor, "org.psi-im.AccountInfoAccessor/0.3")
