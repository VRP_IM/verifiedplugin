#pragma once

#include <QtPlugin>

class QString;
class QStringList;

class AccountInfoAccessingHost
{
public:
    virtual ~AccountInfoAccessingHost()
    {}

    virtual QString status(int account) const = 0;
    virtual QString statusMessage(int account) const = 0;
    virtual QString proxyHost(int account) const = 0;
    virtual int proxyPort(int account) const = 0;
    virtual QString proxyUser(int account) const = 0;
    virtual QString proxyPassword(int account) const = 0;
    virtual QString jid(int account) const = 0;  // if account out of range return "-1"
    virtual QString id(int account) const = 0;  // if account out of range return "-1"
    virtual QString name(int account) const = 0;  // if account out of range return ""
    virtual QStringList roster(int account) const = 0; // if account out of range return List with one element, value "-1"
    virtual int findOnlineAccountForContact(const QString &jid) const = 0; // gets all accounts and searches for specified contact in them. return -1 if account is not found
    virtual int size() const = 0; // returns count of accounts
    virtual int index(const QString &id) const = 0;
};

Q_DECLARE_INTERFACE(AccountInfoAccessingHost, "org.psi-im.AccountInfoAccessingHost/0.2")
