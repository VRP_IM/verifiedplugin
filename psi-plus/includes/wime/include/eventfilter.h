#pragma once

#include <QtPlugin>

#define UN __attribute__ ((unused))

class QDomElement;
class QString;

class EventFilter
{
public:
    virtual ~EventFilter()
    {}

    // true = handled, don't pass to next handler

    virtual bool processEvent(UN int account, UN QDomElement &e) { return false; }
    virtual bool processMessage(UN int account, UN const QString &fromJid, UN const QString &body, UN const QString &subject) { return false; }
    virtual bool processOutgoingMessage(UN int account, UN const QString &fromJid, UN QString &body, UN const QString &type, UN QString &subject) { return false; }
    virtual void login(UN int account) {}
    virtual void logout(UN int account) {}
};

#undef UN

Q_DECLARE_INTERFACE(EventFilter, "org.psi-im.EventFilter/0.2")
