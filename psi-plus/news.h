#pragma once

#include <QDialog>

namespace verified {

namespace Ui {

class News;

}

class News : public QDialog
{
    Q_OBJECT

public:
    explicit News(QWidget *parent = 0);
    ~News();

    void setMessages(const QStringList &messages);

private:
    Ui::News *ui;
};

} // namespace verified
