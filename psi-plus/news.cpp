#include "news.h"
#include "ui_news.h"

#include "common.h"

#include <QRegExp>

namespace verified {

News::News(QWidget *parent)
#ifdef Q_OS_WIN
    : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::MSWindowsFixedSizeDialogHint)
#else
    : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint)
#endif
    , ui(new Ui::News)
{
    ui->setupUi(this);
}

News::~News()
{
    delete ui;
}

void News::setMessages(const QStringList &messages)
{
    QString message = messages.join(QS("<hr>\n"));
    message.replace(QRegExp(QS("(https?://[^\\s<>]+)")), QS("<a href=\"\\1\">\\1</a>"));
    ui->tbNews->setHtml(message);
}

} // namespace verified
