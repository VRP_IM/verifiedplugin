#include "verifiedoptions.h"
#include "ui_verifiedoptions.h"

#include "common.h"

#include <optionaccessinghost.h>

namespace verified {

VerifiedOptions::VerifiedOptions(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::VerifiedOptions)
{
    ui->setupUi(this);

    ui->cmbProxyType->addItem(QS("HTTP \"Connect\""), QS("http"));
    ui->cmbProxyType->addItem(QS("SOCKS Version 5"), QS("socks"));
    ui->cmbProxyType->addItem(QS("HTTP Polling"), QS("poll"));
}

VerifiedOptions::~VerifiedOptions()
{
    delete ui;
}

void VerifiedOptions::applyOptions()
{
    _optionHost->setPluginOption(QS("proxy.type"), ui->cmbProxyType->itemData(ui->cmbProxyType->currentIndex()).toString());
    _optionHost->setPluginOption(QS("proxy.host"), ui->leProxyHost->text());
    _optionHost->setPluginOption(QS("proxy.port"), ui->leProxyPort->text().toInt());
    _optionHost->setPluginOption(QS("proxy.user"), ui->leProxyUsername->text());
    _optionHost->setPluginOption(QS("proxy.pass"), ui->leProxyPass->text());
}

void VerifiedOptions::setApplicationInfoAccessingHost(ApplicationInfoAccessingHost *appInfo)
{
    _appInfo = appInfo;
}

void VerifiedOptions::setOptionAccessingHost(OptionAccessingHost *optionHost)
{
    _optionHost = optionHost;
}

void VerifiedOptions::setPluginPath(const QString &path)
{
    ui->lbPath->setText(path);
}

void VerifiedOptions::setSystem(const QString &system)
{
    ui->lbSystem->setText(system);
}

void VerifiedOptions::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    window()->adjustSize();
    window()->setMinimumWidth(window()->width());
    window()->setMinimumHeight(window()->height());

    Proxy proxy = _appInfo->getProxyFor(QS("Verified"));

    if (!proxy.host.isEmpty()) {
        ui->cmbProxyType->setCurrentIndex(ui->cmbProxyType->findData(proxy.type));
        ui->leProxyHost->setText(proxy.host);
        ui->leProxyPort->setText(QString::number(proxy.port));
        ui->leProxyUsername->setText(proxy.user);
        ui->leProxyPass->setText(proxy.pass);
    }
}

} // namespace verified
