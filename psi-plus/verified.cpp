#include "verified.h"

#include "applicationinfoaccessinghost.h"
#include "blacklist.h"
#include "common.h"
#include "news.h"
#include "qjsonwrapper.h"
#include "update.h"
#include "blacksinhistory.h"

#include <accountinfoaccessinghost.h>
#include <activetabaccessinghost.h>
#include <iconfactoryaccessinghost.h>
#include <optionaccessinghost.h>
#include <psiaccountcontrollinghost.h>
#include <stanzasendinghost.h>

#include <QApplication>
#include <QCryptographicHash>
#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QDomText>
#include <QFile>
#include <QFile>
#include <QMessageBox>
#include <QNetworkProxy>
#include <QNetworkReply>
#include <QRegExp>
#include <QSettings>
#include <QStyle>
#include <QTextEdit>
#include <QTranslator>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
# include <QVersionNumber>
#endif

#define INTERNAL_VERSION 17
#define TIMER_FIRST_CALL (3 * 1000) /* 3 seconds */
#define TIMER_INTERVAL (3 * 60 * 60 * 1000) /* 3 hours */
#define NEWS_TIMER_INTERVAL (10 * 60 * 1000) /* 10 minutes */
#define CHECK_NEW_VERSION_FIRST_CALL (60 * 1000) /* 1 minute */
#define CHECK_NEW_VERSION_INTERVAL (24 * 60 * 60 * 1000) /* 1 day */
#define SCAMMER_DB_URL "http://vrp.im/db/db.json"
#define FAKE_FORUM_DB_URL "http://vrp.im/db/db_forum.json"
#define GOOD_USER_DB_URL "http://vrp.im/db/db_goodusers.json"
#define CHECK_NEW_VERSION_URL "http://vrp.im/psi-verified-appcast.ini"
#define NEWS_URL "http://vrp.im/db/message.json"
#define SILENT_INTERVAL (60 * 60 * 1000) /* 1 hour */
#define SELF_BL_INTERVAL (24 * 60 * 60 * 1000) /* 1 day */

#if defined Q_OS_LINUX
# define SYSTEM "linux"
# define LIB_NAME "libverifiedplugin.so"
#elif defined Q_CC_MSVC
# define SYSTEM "windows_msvc"
# define LIB_NAME "verifiedplugin.dll"
#elif defined Q_OS_WIN && defined Q_CC_GNU
# if QT_VERSION == 0x050501
#  define SYSTEM "windows_mingw_5_5_1"
# elif QT_VERSION == 0x040805
#  define SYSTEM "windows_mingw_4_8_5"
# else
#  define SYSTEM "windows_mingw"
# endif
# define LIB_NAME "libverifiedplugin.dll"
#elif defined Q_OS_MAC
# define SYSTEM "macos"
# define LIB_NAME "libverifiedplugin.so"
#endif

#if defined Q_PROCESSOR_X86_64 || defined QT_ARCH_X86_64 || defined __x86_64 || defined __x86_64__ || defined __amd64 || defined _M_X64
# define ARCHITECTURE "x64"
#elif defined Q_PROCESSOR_X86_32 || defined QT_ARCH_I386 || defined __i386  || defined __i386__ || defined _M_IX86
# define ARCHITECTURE "x32"
#endif

static const char *VERIFIED_FORUM = QT_TRANSLATE_NOOP("verified::Verified",
                                                      "Verified forum");

static const char *NONASCII_MESSAGE = QT_TRANSLATE_NOOP("verified::Verified",
                                                        "<big><b>Attention!</b> You are communcating with person who uses NON-Latin characters in his Jabber ID. "
                                                        "Usually scammers do this. Read here <font color=\"blue\">/showthread.php?t=7336</font> (Verified forum) for details.</big>");

static const char *LIKE_USERNAME_MESSAGE = QT_TRANSLATE_NOOP("verified::Verified",
                                                             "<big><b>Attention!</b> You are communcating with person whose username looks like a Jabber ID but not identical with his real Jabber ID. Be careful!</big>");

static const char *SCAMMER_MESSAGE = QT_TRANSLATE_NOOP("verified::Verified",
                                                       "<big><b>Attention!</b> You are communcating with person who has blacks. Read here for details:<br/>%1</big>");

static const char *GOOD_USER_MESSAGE = QT_TRANSLATE_NOOP("verified::Verified",
                                                         "<big><b>Attention!</b> You are communicating with person who has registration on Verified forum<br>"
                                                         "Login: %1<br>"
                                                         "Registration date: %2<br>"
                                                         "Link to profile: <font color=\"blue\">/member.php?u=%3</font></big>");

static const char *FAKE_FORUM_MESSAGE = QT_TRANSLATE_NOOP("verified::Verified",
                                                          "<big><b>Attention!</b> Just now you or your parthner send a link which contained in our fake forums database. Be careful! "
                                                          "To known more open Verified forum and type in search box this URL.</big>");

namespace verified {

#ifndef HAVE_QT5
Q_EXPORT_PLUGIN2(Verified, Verified)
#endif

Verified::Verified()
    : _enabled()
    , _accountHost()
    , _optionHost()
    , _stanzaSending()
    , _accountInfo()
    , _appInfo(new ApplicationInfoAccessingHost(this))
    , _contactInfo()
    , _iconFactory()
    , _activeTab()
    , _menu()
    , _nam()
    , _timer(new QTimer(this))
    , _newsTimer(new QTimer(this))
    , _optionsForm()
    , _checkNewVersionTimer(new QTimer(this))
{
    _timer->setInterval(TIMER_INTERVAL);
    _timer->setSingleShot(true);
    connect(_timer, SIGNAL(timeout()), SLOT(updateScammerDb()));
    connect(_timer, SIGNAL(timeout()), SLOT(updateFakeForumDb()));
    connect(_timer, SIGNAL(timeout()), SLOT(updateGoodUserDb()));

    _newsTimer->setInterval(NEWS_TIMER_INTERVAL);
    _newsTimer->setSingleShot(true);
    connect(_newsTimer, SIGNAL(timeout()), SLOT(checkNews()));

    _checkNewVersionTimer->setInterval(CHECK_NEW_VERSION_INTERVAL);
    _checkNewVersionTimer->setSingleShot(false);
    connect(_checkNewVersionTimer, SIGNAL(timeout()), SLOT(checkNewVersion()));

    breakingAbiList << QS("1.4.1323");
}

Verified::~Verified()
{
    delete _menu;
}

QString Verified::version() const
{
    return QString::number(INTERNAL_VERSION);
}

QWidget *Verified::options()
{
    if (!_enabled) {
        return 0;
    }

    _optionsForm = new VerifiedOptions();
    _optionsForm->setPluginPath(QDir::toNativeSeparators(pluginPath()));
    _optionsForm->setSystem(updateGroup());
    _optionsForm->setApplicationInfoAccessingHost(_appInfo);
    _optionsForm->setOptionAccessingHost(_optionHost);
    return qobject_cast<QWidget*>(_optionsForm);
}

bool Verified::enable()
{
    _enabled = true;

    QFile::remove(pluginPath() + QS(".old"));

    QString defaultLang = QLocale().name().section('_', 0, 0);
    QSettings settings(_appInfo->appHomeDir(ApplicationInfoAccessingHost::ConfigLocation) + QS("/psirc"), QSettings::IniFormat);
    QString lang = settings.value(QS("last_lang")).toString();
    if (lang.isEmpty()) {
        lang = defaultLang;
    }
    if (_translator.load(QLocale(lang), QS("verified"), QS("_"), QS(":/translations"))) {
        qApp->installTranslator(&_translator);
    }

    // Protect from incorrect plugin path. Plugin can be updated only if it located at correct path.
    if (!QFile::exists(pluginPath())) {
        QMessageBox::critical(0, name(), tr("Plugin not found at correct path. Please move it to %1.").arg(QDir::toNativeSeparators(pluginPath())));

        _enabled = false;
        return _enabled;
    }

    QFile file(":/icons/verified.png");
    file.open(QIODevice::ReadOnly);
    QByteArray image = file.readAll();
    _iconFactory->addIcon("verified/icon", image);
    file.close();

    Proxy psiProxy = _appInfo->getProxyFor(name());
    QNetworkProxy::ProxyType type;
    if (psiProxy.type == "socks") {
        type = QNetworkProxy::Socks5Proxy;
    }
    else {
        type = QNetworkProxy::HttpProxy;
    }

    QNetworkProxy proxy(type, psiProxy.host, psiProxy.port, psiProxy.user, psiProxy.pass);

    _nam = new QNetworkAccessManager(this);
    if (!proxy.hostName().isEmpty()) {
        _nam->setProxy(proxy);
    }

    _checkNewVersionTimer->start(CHECK_NEW_VERSION_FIRST_CALL);
    QTimer::singleShot(TIMER_FIRST_CALL, this, SLOT(updateScammerDb()));
    QTimer::singleShot(TIMER_FIRST_CALL, this, SLOT(updateFakeForumDb()));
    QTimer::singleShot(TIMER_FIRST_CALL, this, SLOT(updateGoodUserDb()));
    QTimer::singleShot(TIMER_FIRST_CALL, this, SLOT(checkNews()));

#ifdef WIME
    QRegExp rx(QS("^(\\d+)\\.(\\d+)"));
    rx.indexIn(_appInfo->appVersion());
    int majorVersion = rx.cap(1).toInt();
    int minorVersion = rx.cap(2).toInt();
    if (majorVersion && (majorVersion > 1 || minorVersion > 4)) {
        _optionHost->setGlobalOption(QS("options.messages.check-non-latin-jids"), false);
    }
#endif

    // Fix not tabs highlighting
     _optionHost->setGlobalOption(QS("options.ui.chat.css"), QString());

     QSqlDatabase db = QSqlDatabase::addDatabase(QS("QSQLITE"), QS("vr-plugin"));
     db.setDatabaseName(_appInfo->appHistoryDir() + QS("/history.db"));
     db.setConnectOptions(QS("QSQLITE_OPEN_READONLY"));

    return _enabled;
}

bool Verified::disable()
{
    _timer->stop();
    _checkNewVersionTimer->stop();
    _enabled = false;
    _nam->deleteLater();
    _nam = 0;

#ifdef WIME
    foreach (const QWidget * widget, QApplication::topLevelWidgets()) {
        if (widget->objectName() == QS("OptionsUI")) {
            QRegExp rx(QS("^(\\d+)\\.(\\d+)"));
            rx.indexIn(_appInfo->appVersion());
            int majorVersion = rx.cap(1).toInt();
            int minorVersion = rx.cap(2).toInt();
            if (majorVersion && (majorVersion > 1 || minorVersion > 4)) {
                _optionHost->setGlobalOption(QS("options.messages.check-non-latin-jids"), true);
            }
            break;
        }
    }
#endif

    QSqlDatabase::removeDatabase(QS("vr-plugin"));

    return true;
}

void Verified::applyOptions()
{
    _optionsForm->applyOptions();
}

void Verified::restoreOptions()
{
}

QPixmap Verified::icon() const
{
    return QPixmap(":/icons/verified.png");
}

QString Verified::pluginInfo()
{
    return QString();
}

bool Verified::incomingStanza(int account, const QDomElement &stanza)
{
    if (!_enabled) {
        return false;
    }

    handleStanza(account, stanza, true);

    return false;
}

bool Verified::outgoingStanza(int account, QDomElement &stanza)
{
    if (!_enabled) {
        return false;
    }

    handleStanza(account, stanza, false);

    return false;
}

void Verified::setOptionAccessingHost(OptionAccessingHost *host)
{
    _optionHost = host;
    _appInfo->setOptionAccessingHost(host);
}

void Verified::setApplicationInfoAccessingHost(::ApplicationInfoAccessingHost *host)
{
     _appInfo->setNativeApplicationInfoAccessingHost(host);
}

bool Verified::processEvent(int account, QDomElement &e)
{
    Q_UNUSED(account);
    Q_UNUSED(e);

    return false;
}

bool Verified::processMessage(int account, const QString &fromJid, const QString &body, const QString &subject)
{
    Q_UNUSED(subject);

    if (!_enabled) {
        return false;
    }

    foreach (const QString &str, _fakeForumList) {
        if (body.contains(str, Qt::CaseInsensitive)) {
            appendSysMsg(account, fromJid, qApp->translate("verified::Verified", FAKE_FORUM_MESSAGE));
            break;
        }
    }

    return false;
}

bool Verified::processOutgoingMessage(int account, const QString &fromJid, QString &body, const QString &type, QString &subject)
{
    Q_UNUSED(type);
    Q_UNUSED(subject);

    if (!_enabled) {
        return false;
    }

    foreach (const QString &str, _fakeForumList) {
        if (body.contains(str, Qt::CaseInsensitive)) {
            appendSysMsg(account, fromJid, qApp->translate("verified::Verified", FAKE_FORUM_MESSAGE));
            break;
        }
    }

    return false;
}

void Verified::logout(int account)
{
    Q_UNUSED(account);
}

void Verified::setupChatTab(QWidget *tab, int account, const QString &contact)
{
    deleteFromChats(tab);

    QString jid = contact.split(QC('/')).first();

    Chat chat;
    chat.alerted = false;
    chat.alertedGood = false;
    chat.messageShowed = false;
    chat.tab = tab;
    chat.jid = jid;
    chat.account = account;
    _chatHash.insert(jid, chat);

    static QString styleSheet = QS("#ChatDlg[scammer=\"true\"] #topFrame { background: #fd5e53 }\n"
                                   "#ChatDlg[scammer=\"true\"] #bottomFrame { background: #fd5e53 }\n"
                                   "#ChatDlg[scammer=\"true\"] #splitter QSplitter::handle { background-color: #fd5e53 }\n"
                                   "#ChatDlg[scammer=\"true\"] #splitter QSplitter { background: #fd5e53 }"
                                   "#ChatDlg[gooduser=\"true\"] #topFrame { background: #abebc6 }\n"
                                   "#ChatDlg[gooduser=\"true\"] #bottomFrame { background: #abebc6 }\n"
                                   "#ChatDlg[gooduser=\"true\"] #splitter QSplitter::handle { background-color: #abebc6 }\n"
                                   "#ChatDlg[gooduser=\"true\"] #splitter QSplitter { background: #abebc6 }");
    tab->setStyleSheet(styleSheet);

    if (isScammer(account, jid)) {
        highlightTab(jid, true);
    }
    else if (isGoodUser(jid)) {
        highlightGoodTab(jid, true);
    }

    connect(tab, SIGNAL(destroyed(QObject*)), SLOT(deleteFromChats(QObject*)));
}

void Verified::setupGCTab(QWidget *tab, int account, const QString &contact)
{
    Q_UNUSED(tab);
    Q_UNUSED(account);
    Q_UNUSED(contact);
}

bool Verified::appendingChatMessage(int account, const QString &contact, QString &body, QDomElement &html, bool local)
{
    Q_UNUSED(account);
    Q_UNUSED(contact);
    Q_UNUSED(body);
    Q_UNUSED(html);
    Q_UNUSED(local);

    return false;
}

void Verified::handleStanza(int account, const QDomElement &stanza, bool incoming)
{
    if (stanza.tagName() != QS("message") || stanza.attribute(QS("type")) != QS("chat")) {
        return;
    }

    QString from = incoming ? stanza.attribute(QS("from")) : stanza.attribute(QS("to"));
    QString jid = from.split(QC('/')).first();

    // Create chat if need
    bool foundChat = false;
    foreach (const Chat &chat, _chatHash) {
        if (chat.jid == jid && chat.account == account) {
            foundChat = true;
            break;
        }
    }

    if (!foundChat && _activeTab->getJid().split(QS("/")).first() == jid) {
        QWidget *chatDlg = _activeTab->getEditBox();
        while (chatDlg && chatDlg->objectName() != QS("ChatDlg")) {
            chatDlg = chatDlg->parentWidget();
        }

        if (chatDlg) {
            foundChat = true;
            setupChatTab(chatDlg, account, _activeTab->getJid());
        }
    }

    if (!foundChat) {
        return;
    }

    QMutableHashIterator<QString, Chat> it = _chatHash;
    while (it.hasNext()) {
        Chat &chat = it.next().value();
        if (chat.jid == jid && chat.account == account) {
            if (chat.messageShowed) {
                return;
            }
            else {
                chat.messageShowed = true;
            }
            break;
        }
    }

    // Check for non Latin characters in JID
    if (checkNonLatin(jid)) {
        appendSysMsg(account, from, qApp->translate("verified::Verified", NONASCII_MESSAGE));
    }

    // Check for like jid username
    if (checkLikeUsername(account, jid)) {
        highlightTab(jid, true);
        appendSysMsg(account, from, qApp->translate("verified::Verified", LIKE_USERNAME_MESSAGE));
    }

    if (!isScammer(account, jid) && isGoodUser(jid)) {
        highlightGoodTab(jid, true);
        GoodUser gu = _goodUserHash.value(jid);
        appendSysMsg(account, jid, qApp->translate("verified::Verified", GOOD_USER_MESSAGE).arg(gu.login, gu.registered, QString::number(gu.id)));
    }

    // Check for ripper
    QStringList blackList;
    foreach (const QString &url, _scammerHash.values(jid)) {
        blackList << url;
    }

    if (!blackList.isEmpty()) {
        appendSysMsg(account, from, qApp->translate("verified::Verified", SCAMMER_MESSAGE).arg(blackList.join(QS("<br/>"))));
    }
}

void Verified::updateScammerDb()
{
    QNetworkRequest request(QString(SCAMMER_DB_URL));
    request.setRawHeader("User-Agent", "Verified Plugin (Psi+)");
    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(finished()), SLOT(parseScammersDb()));
}

void Verified::parseScammersDb()
{
    _timer->start();

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    // Occurs error
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << qPrintable(name()) << reply->errorString();
        reply->close();
        return;
    }

    QMap<QString, SelfBlack> selfBlacks;
    for (int i = 0; !accountJid(i).isEmpty(); ++i) {
        SelfBlack selfBlack;
        selfBlack.count = 0;
        selfBlack.jid = accountJid(i);
        selfBlacks.insert(selfBlack.jid, selfBlack);
    }

    QStringList oldSelfBlacks = _optionHost->getPluginOption(QS("self-black-list")).toStringList();
    foreach (const QString &str, oldSelfBlacks) {
        SelfBlack selfBlack;
        selfBlack.count = str.section(QS(":"), 0, 0).toInt();
        selfBlack.dateTime = QDateTime::fromMSecsSinceEpoch(str.section(QS(":"), 1, 1).toLongLong());
        selfBlack.jid = str.section(QS(":"), 2);

        if (selfBlacks.contains(selfBlack.jid)) {
            selfBlacks.insert(selfBlack.jid, selfBlack);
        }
    }

    // No errors
    QByteArray ba = reply->readAll();

    QVariantList jsonList = QJsonWrapper::parseJson(ba).toList();
    foreach (const QVariant &variant, jsonList) {
        QVariantMap map = variant.toMap();
        if (map.value(QS("type")) != QS("table") || map.value(QS("name")) != QS("rippers")) {
            continue;
        }

        QVariantList scammerList = map.value(QS("data")).toList();
        if (scammerList.isEmpty()) {
            break;
        }

        _scammerHash.clear();
        foreach (const QVariant &item, scammerList) {
            QString jid = item.toMap().value(QS("j")).toString();
            QString url = QSS("<font color=\"blue\">/showthread.php?t=%1</font> (%2)").arg(item.toMap().value(QS("t")).toString(), qApp->translate("verified::Verified", VERIFIED_FORUM));
            _scammerHash.insert(jid, url);
            highlightTab(jid, true);
        }
    }

    QList<QString> jids = _scammerHash.keys();

    foreach (const QString &jid, jids) {
        if (selfBlacks.contains(jid)) {
            SelfBlack sb = selfBlacks.value(jid);
            sb.urls = _scammerHash.values(jid);
            if (sb.count < 2 && (sb.dateTime.isNull() || sb.dateTime.msecsTo(QDateTime::currentDateTimeUtc()) > SELF_BL_INTERVAL)) {
                sb.count++;
                sb.dateTime = QDateTime::currentDateTimeUtc();
                BlackList bl(sb.urls);
                bl.exec();
            }
            selfBlacks.insert(jid, sb);
        }
    }

    QStringList newBl;
    foreach (const SelfBlack &sf, selfBlacks) {
        if (!sf.urls.isEmpty()) {
            newBl << QSS("%1:%2:%3").arg(QString::number(sf.count), QString::number(sf.dateTime.toMSecsSinceEpoch()), sf.jid);
        }
    }

    _optionHost->setPluginOption(QS("self-black-list"), newBl);

    QHashIterator<QString, Chat> it = _chatHash;
    while (it.hasNext()) {
        it.next();
        Chat chat = it.value();
        if (!checkNonLatin(chat.jid) && !checkLikeUsername(chat.account, chat.jid) && chat.alerted && !_scammerHash.keys().contains(chat.jid)) {
            highlightTab(chat.jid, false);
        }
    }

    checkScammersInHistory();
}

void Verified::updateFakeForumDb()
{
    QNetworkRequest request(QString(FAKE_FORUM_DB_URL));
    request.setRawHeader("User-Agent", "Verified Plugin (Psi+)");
    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(finished()), SLOT(parseFakeForumDb()));
}

void Verified::parseFakeForumDb()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    // Occurs error
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << qPrintable(name()) << reply->errorString();
        reply->close();
        return;
    }

    // No errors
    QByteArray ba = reply->readAll();

    QVariantList jsonList = QJsonWrapper::parseJson(ba).toList();
    foreach (const QVariant &variant, jsonList) {
        QVariantMap map = variant.toMap();
        if (map.value(QS("type")) != QS("table") || map.value(QS("name")) != QS("rippers")) {
            continue;
        }

        QVariantList fakeForumList = map.value(QS("data")).toList();
        if (fakeForumList.isEmpty()) {
            break;
        }

        _fakeForumList.clear();
        foreach (const QVariant &item, fakeForumList) {
            _fakeForumList << item.toMap().value(QS("l")).toString();
        }
    }
}

void Verified::checkNews()
{
    QNetworkRequest request(QString(NEWS_URL));
    request.setRawHeader("User-Agent", "Verified Plugin (Psi+)");
    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(finished()), SLOT(showNews()));
}

void Verified::showNews()
{
    _newsTimer->start();

    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    // Occurs error
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << qPrintable(name()) << reply->errorString();
        reply->close();
        return;
    }

    // No errors
    bool ok;
    QByteArray ba = reply->readAll();

    QVariantList jsonList = QJsonWrapper::parseJson(ba, &ok).toList();

    if (!ok) {
        return;
    }

    QStringList messages;
    QStringList prevHashes = _optionHost->getPluginOption(QS("news-hashes")).toString().split(QS(";"));
    QStringList hashes;
    foreach (const QVariant &variant, jsonList) {
        QVariantMap map = variant.toMap();

        QString message = map.value(QS("message")).toString();
        QString expiresStr = map.value(QS("expires")).toString();
        expiresStr.replace(QS(" "), QS("T"));
        expiresStr += QS("Z");
        QDateTime expires = QDateTime::fromString(expiresStr, Qt::ISODate);
        QCryptographicHash hashAlgo(QCryptographicHash::Md5);
        hashAlgo.addData(message.toUtf8());
        hashes << QString::fromLatin1(hashAlgo.result().toHex());

        if (prevHashes.contains(hashes.last()) || message.isEmpty() || (expires.isValid() && expires < QDateTime::currentDateTimeUtc())) {
            continue;
        }

        messages << message;
    }

    _optionHost->setPluginOption(QS("news-hashes"), hashes.join(QS(";")));

    if (!messages.isEmpty()) {
        News dlg;
        dlg.setMessages(messages);
        dlg.exec();
    }
}

void Verified::updateGoodUserDb()
{
    QNetworkRequest request(QString(GOOD_USER_DB_URL));
    request.setRawHeader("User-Agent", "Verified Plugin (Psi+)");
    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(finished()), SLOT(parseGoodUserDb()));
}

void Verified::parseGoodUserDb()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    // Occurs error
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << qPrintable(name()) << reply->errorString();
        reply->close();
        return;
    }

    // No errors
    QByteArray ba = reply->readAll();

    QVariantList jsonList = QJsonWrapper::parseJson(ba).toList();
    foreach (const QVariant &variant, jsonList) {
        QVariantMap map = variant.toMap();
        if (map.value(QS("type")) != QS("table") || map.value(QS("name")) != QS("goodusers")) {
            continue;
        }

        QVariantList goodUserList = map.value(QS("data")).toList();
        if (goodUserList.isEmpty()) {
            break;
        }

        _goodUserHash.clear();
        foreach (const QVariant &item, goodUserList) {
            GoodUser gu;
            gu.jid = item.toMap().value(QS("j")).toString();
            gu.login = item.toMap().value(QS("l")).toString();
            gu.id  = item.toMap().value(QS("i")).toInt();
            gu.registered = item.toMap().value(QS("r")).toString();
            _goodUserHash.insert(gu.jid, gu);
            highlightGoodTab(gu.jid, true);
        }
    }
}

void Verified::actionActivated()
{
    delete _menu;
    _menu = new QMenu();
    _menu->addAction(tr("Remind me in 1 hour"), this, SLOT(addToVerified()));
    _menu->popup(QCursor::pos());
}

void Verified::addToVerified()
{
    QString jid = _activeTab->getJid().split(QLatin1Char('/')).first();
    _silentHash.insert(jid, QDateTime::currentDateTime());
}

void Verified::checkNewVersion()
{
    _checkNewVersionTimer->start(CHECK_NEW_VERSION_INTERVAL);
    QNetworkRequest request(QString(CHECK_NEW_VERSION_URL));
    request.setRawHeader("User-Agent", "Verified Plugin (Psi+)");
    QNetworkReply *reply = _nam->get(request);
    connect(reply, SIGNAL(finished()), SLOT(parseNewVersion()));
}

void Verified::parseNewVersion()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    // Occurs error
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Verified Plugin:" << reply->errorString();
        reply->close();
        return;
    }

    // No errors
    QByteArray ba = reply->readAll();

    QString fileName = _appInfo->appHomeDir(ApplicationInfoAccessingHost::CacheLocation) + QS("/psi-verified-appcast.ini");
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        return;
    }

    file.write(ba);
    file.close();

    QSettings settings(fileName, QSettings::IniFormat);

    settings.beginGroup(updateGroup());
    int version = settings.value(QS("version")).toInt();
    int skippedVersion = _optionHost->getPluginOption(QS("verified")).toInt();
    QString url = settings.value(QS("url")).toString();

    if (url.isEmpty() || INTERNAL_VERSION >= version || skippedVersion == version) {
        return;
    }

    Update dlg(version, INTERNAL_VERSION);
    switch (dlg.exec()) {
    case Update::SkipVersion:
        _optionHost->setPluginOption(QS("verified"), version);
        break;

    case Update::InstallUpdate: {
        QNetworkRequest request(url);
        request.setRawHeader("User-Agent", "Verified Plugin (Psi+)");
        reply = _nam->get(request);
        connect(reply, SIGNAL(finished()), SLOT(updateVersion()));
        break;
    }

    case Update::RemindLater:
        break;
    }
}

void Verified::updateVersion()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());

    // Occurs error
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Verified Plugin:" << reply->errorString();
        reply->close();
        return;
    }

    // No errors
    QByteArray ba = reply->readAll();

    QDir(pluginPath().section(QC('/'), 0, -3)).mkdir(QS("plugins"));
    QFile file(pluginPath() + QS(".new"));
    if (!file.open(QIODevice::WriteOnly)) {
        return;
    }

    file.write(ba);
    file.close();

    if (QFile::exists(pluginPath()) && !QFile::rename(pluginPath(), pluginPath() + QS(".old"))) {
        return;
    }

    QFile::rename(pluginPath() + QS(".new"), pluginPath());

    QMessageBox::information(0, tr("Verified Plugin Update"), tr("Installed a new version.<br/>Need to restart application."), QMessageBox::Ok, QMessageBox::Ok);
}

void Verified::deleteFromChats(QObject *object)
{
    QMutableHashIterator<QString, Chat> it = _chatHash;

    while (it.hasNext()) {
        it.next();
        if (it.value().tab == object) {
            it.remove();
            break;
        }
    }
}

QString Verified::accountJid(int account) const
{
#ifdef WIME
    QString jid = _accountInfo->jid(account);
#else
    QString jid = _accountInfo->getJid(account);
#endif

    if (jid == QS("-1")) {
        jid = QString();
    }

    return jid;
}

bool Verified::isScammer(int account, const QString &contact)
{
    return checkLikeUsername(account, contact) || checkNonLatin(contact) || _scammerHash.contains(contact);
}

bool Verified::isGoodUser(const QString &contact)
{
    return _goodUserHash.keys().contains(contact);
}

bool Verified::checkNonLatin(const QString &contact)
{
    bool res = false;
    for (int i = 0; i < contact.length(); i++) {
        QChar c = contact[i];
        if (c.toLatin1() == 0 && c.category() != QChar::Symbol_Currency) {
            res = true;
            break;
        }
    }
    return res;
}

bool Verified::checkLikeUsername(int account, const QString &contact)
{
    QStringList nameParts = _contactInfo->name(account, contact).split(QC(' '));
    bool res = false;
    foreach (const QString &part, nameParts) {
        if (part.trimmed().isEmpty()) {
            continue;
        }

        if (part.contains(QRegExp(QS(".+@.+\\..+"))) && contact.compare(part, Qt::CaseInsensitive)) {
            res = true;
            break;
        }
    }
    return res;
}

QString Verified::pluginPath() const
{
    return _appInfo ? _appInfo->appHomeDir(ApplicationInfoAccessingHost::DataLocation) + QS("/plugins/") + QS(LIB_NAME) : QString();
}

QString Verified::updateGroup() const
{
    QString appName = _appInfo->appName().contains(QS("Wime")) ? QS("wime") : QS("psi+");

#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    if (appName == QS("psi+")) {
        QString version = _appInfo->appVersion();
        version.remove(QRegExp("^psi+"));
        version = version.section(QC(' '), 0, 0).trimmed();
        QVersionNumber versionNumber = QVersionNumber::fromString(version);

        for (const QString &breakingAbi: breakingAbiList) {
            QVersionNumber breakingVersion = QVersionNumber::fromString(breakingAbi);
            if (breakingVersion <= versionNumber) {
                appName += QS("-") + breakingAbi;
                break;
            }
        }
    }
#endif

#ifdef Q_OS_MAC
    return QSS("%1-%2").arg(appName, QS(SYSTEM));
#else
    return QSS("%1-%2-%3").arg(appName, QS(SYSTEM), QS(ARCHITECTURE));
#endif
}

void Verified::highlightTab(const QString &jid, bool enable)
{
    for (QHash<QString, Chat>::iterator it = _chatHash.find(jid); it != _chatHash.end() && it.key() == jid; ++it) {
        Chat &chat = *it;
        if (chat.alerted != enable) {
            chat.alerted = enable;
            chat.tab->setProperty("scammer", enable);
            chat.tab->setProperty("gooduser", false);

            // Style sheet wont be recalculated after dynamic property changes
            // see https://forum.qt.io/topic/81644/stylesheet-recomputing-after-property-change
            chat.tab->setStyleSheet(chat.tab->styleSheet());
        }
    }
}

void Verified::highlightGoodTab(const QString &jid, bool enable)
{
    for (QHash<QString, Chat>::iterator it = _chatHash.find(jid); it != _chatHash.end() && it.key() == jid; ++it) {
        Chat &chat = *it;
        if (chat.tab->property("scammer").toBool()) {
            continue;
        }

        if (chat.alertedGood != enable) {
            chat.alertedGood = enable;
            chat.tab->setProperty("gooduser", enable);

            // Style sheet wont be recalculated after dynamic property changes
            // see https://forum.qt.io/topic/81644/stylesheet-recomputing-after-property-change
            chat.tab->setStyleSheet(chat.tab->styleSheet());
        }
    }
}

bool Verified::isSilent(const QString &jid)
{
    QDateTime dt = _silentHash.value(jid);
    bool res = false;
    if (dt.isValid()) {
        if (dt.msecsTo(QDateTime::currentDateTime()) < SILENT_INTERVAL) {
            res = true;
        }
        else {
            _silentHash.remove(jid);
        }
    }
    return res;
}

bool Verified::appendSysMsg(int account, const QString &jid, const QString &message)
{
#ifdef HAVE_APPEND_SYS_HTML_MSG
    return _accountHost->appendSysHtmlMsg(account, jid, message);
#else
    return _accountHost->appendSysMsg(account, jid, message);
#endif
}

void Verified::checkScammersInHistory() const
{
    QSqlDatabase db = QSqlDatabase::database(QS("vr-plugin"));

    QSqlQuery query(db);
    query.prepare(QS(
                      "SELECT DISTINCT jid "
                      "FROM events "
                      "JOIN contacts  "
                      "ON events.contact_id = contacts.id "
                      "WHERE date > ?"
                  ));
    QDate date = QDate::currentDate().addDays(-30);
    query.addBindValue(date.toString(QS("yyyy-MM-dd")));
    query.exec();

    QMultiHash<QString, QString> blacks;

    QStringList historyBlacks = _optionHost->getPluginOption(QS("history-blacks")).toStringList();

    while (query.next()) {
        QString jid = query.value(0).toString();

        if (historyBlacks.contains(jid)) {
            continue;
        }

        foreach (const QString &thread, _scammerHash.values(jid)) {
            historyBlacks << jid;
            QString str = thread;
            str.replace(QRegExp(QS("\\([^)]+\\)$")), QString());
            blacks.insert(jid, str);
        }
    }

    db.close();

    historyBlacks.removeDuplicates();
    _optionHost->setPluginOption(QS("history-blacks"), historyBlacks);


    if (!blacks.isEmpty()) {
        BlacksInHistory dlg;
        dlg.setScammers(blacks);
        dlg.exec();
    }
}

} // namespace verified
