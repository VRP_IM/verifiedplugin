#pragma once

#include "verifiedoptions.h"

#include <accountinfoaccessor.h>
#include <activetabaccessor.h>
#include <applicationinfoaccessor.h>
#include <chattabaccessor.h>
#include <contactinfoaccessinghost.h>
#include <contactinfoaccessor.h>
#include <eventfilter.h>
#include <iconfactoryaccessor.h>
#include <optionaccessor.h>
#include <plugininfoprovider.h>
#include <psiaccountcontroller.h>
#include <psiplugin.h>
#include <stanzafilter.h>
#include <stanzasender.h>

#include <QDateTime>
#include <QHash>
#include <QMultiHash>
#include <QList>
#include <QMenu>
#include <QNetworkAccessManager>
#include <QStringList>
#include <QTimer>
#include <QTranslator>

class ApplicationInfoAccessingHost;

namespace verified {

class ApplicationInfoAccessingHost;

class Verified : public QObject
               , public PsiPlugin
               , public PluginInfoProvider
               , public StanzaFilter
               , public PsiAccountController
               , public OptionAccessor
               , public StanzaSender
               , public AccountInfoAccessor
               , public ApplicationInfoAccessor
               , public ContactInfoAccessor
               , public IconFactoryAccessor
               , public ActiveTabAccessor
               , public EventFilter
               , public ChatTabAccessor
{
    Q_OBJECT
#ifdef HAVE_QT5
    Q_PLUGIN_METADATA(IID "com.psi-plus.Verified" FILE "psiplugin.json")
#endif
    Q_INTERFACES(PsiPlugin
                 PluginInfoProvider
                 StanzaFilter
                 PsiAccountController
                 OptionAccessor
                 StanzaSender
                 AccountInfoAccessor
                 ApplicationInfoAccessor
                 ContactInfoAccessor
                 IconFactoryAccessor
                 ActiveTabAccessor
                 EventFilter
                 ChatTabAccessor)

public:
    Verified();
    ~Verified();

    // from PsiPlugin
    QString name() const { return "Verified"; }
    QString shortName() const { return "verified"; }
    QString version() const;

    QWidget *options();
    bool enable();
    bool disable();
    void applyOptions();
    void restoreOptions();
    QPixmap icon() const;

    // from PluginInfoProvider
    QString pluginInfo();

    // from StanzaSender
    void setStanzaSendingHost(StanzaSendingHost *host) { _stanzaSending = host; }

    // from StanzaFilter
    bool incomingStanza(int account, const QDomElement &stanza);
    bool outgoingStanza(int account, QDomElement &stanza);

    // from PsiAccountController
    void setPsiAccountControllingHost(PsiAccountControllingHost *host) { _accountHost = host; }

    // from OptionAccessor
    void setOptionAccessingHost(OptionAccessingHost *host);

    void optionChanged(const QString& /*option*/) {}

    // from AccountInfoAccessor
    void setAccountInfoAccessingHost(AccountInfoAccessingHost *host) { _accountInfo = host; }

    // from ApplicationInfoAccessor
    void setApplicationInfoAccessingHost(::ApplicationInfoAccessingHost *host);

    // from ContactInfoAccessor
    void setContactInfoAccessingHost(ContactInfoAccessingHost *host) { _contactInfo = host; }

    // from IconFactoryAccessor
    void setIconFactoryAccessingHost(IconFactoryAccessingHost *host) { _iconFactory = host; }

    // from ActiveTabAccessor
    void setActiveTabAccessingHost(ActiveTabAccessingHost *host) { _activeTab = host; }

    // from EventFilter
    bool processEvent(int account, QDomElement &e);
    bool processMessage(int account, const QString &fromJid, const QString &body, const QString &subject);
    bool processOutgoingMessage(int account, const QString &fromJid, QString &body, const QString &type, QString &subject);
    void logout(int account);

    // ChatTabAccessor
    void setupChatTab(QWidget *tab, int account, const QString &contact);
    void setupGCTab(QWidget *tab, int account, const QString &contact);
    bool appendingChatMessage(int account, const QString &contact, QString &body, QDomElement &html, bool local);

    void handleStanza(int account, const QDomElement &stanza, bool incoming);

public slots:
    void updateScammerDb();
    void parseScammersDb();

    void updateFakeForumDb();
    void parseFakeForumDb();

    void checkNews();
    void showNews();

    void updateGoodUserDb();
    void parseGoodUserDb();

    void actionActivated();
    void addToVerified();
    void checkNewVersion();
    void parseNewVersion();
    void updateVersion();

    void deleteFromChats(QObject *object);

private:
    QString accountJid(int account) const;
    bool isScammer(int account, const QString &contact);
    bool isGoodUser(const QString &contact);
    bool checkNonLatin(const QString &contact);
    bool checkLikeUsername(int account, const QString &contact);
    QAction *createAction(QObject *parent, int account, const QString &contact);
    void updateToolTip(QAction *action);
    QString pluginPath() const;
    QString updateGroup() const;
    void highlightTab(const QString &jid, bool enable);
    void highlightGoodTab(const QString &jid, bool enable);
    bool isSilent(const QString &jid);
    bool appendSysMsg(int account, const QString &jid, const QString &message);
    void checkScammersInHistory() const;

    bool _enabled;
    PsiAccountControllingHost *_accountHost;
    OptionAccessingHost *_optionHost;
    StanzaSendingHost *_stanzaSending;
    AccountInfoAccessingHost *_accountInfo;

    ApplicationInfoAccessingHost *_appInfo;
    ContactInfoAccessingHost *_contactInfo;
    IconFactoryAccessingHost *_iconFactory;
    ActiveTabAccessingHost *_activeTab;

    QMenu *_menu;
    QNetworkAccessManager *_nam;
    QTimer *_timer;
    QTimer *_newsTimer;
    VerifiedOptions *_optionsForm;

    QMultiHash<QString, QString> _scammerHash;
    QHash<QString, QAction*> _actions;
    QTranslator _translator;
    QTimer *_checkNewVersionTimer;
    QStringList _fakeForumList;

    struct Chat
    {
        QWidget *tab;
        QString jid;
        int account;
        bool alerted;
        bool alertedGood;
        bool messageShowed;
    };

    struct SelfBlack
    {
        int count;
        QDateTime dateTime;
        QString jid;
        QStringList urls;
    };

    struct GoodUser
    {
        int id;
        QString login;
        QString jid;
        QString registered;
    };

    QMultiHash<QString, Chat> _chatHash;
    QHash<QString, QDateTime> _silentHash;
    QMultiHash<QString, GoodUser> _goodUserHash;
    QStringList breakingAbiList;
};

} // namespace verified
