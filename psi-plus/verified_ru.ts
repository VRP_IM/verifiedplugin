<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>QJson::ParserRunnable</name>
    <message>
        <source>An error occurred while parsing json: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>verified::BlackList</name>
    <message>
        <source>&lt;b&gt;&lt;font color=&quot;red&quot;&gt;Attention!&lt;/font&gt;&lt;/b&gt; You got a BlackList. Please respond to it. Link to topic:</source>
        <translation>&lt;b&gt;&lt;font color=&quot;red&quot;&gt;Внимание!&lt;/font&gt;&lt;/b&gt; На вас написан BlackList. Пожалуйста отреагируйте на него, ссылка на топик:</translation>
    </message>
</context>
<context>
    <name>verified::BlacksInHistory</name>
    <message>
        <source>Verified Plugin Message</source>
        <translation>Verified Plugin Сообщение</translation>
    </message>
    <message>
        <source>In last 30 days you had conversation with persons who get blacks.</source>
        <translation>За последние 30 дней вы общались с людьми на которых есть блеки.</translation>
    </message>
</context>
<context>
    <name>verified::News</name>
    <message>
        <source>Verified Plugin Message</source>
        <translation>Verified Plugin Сообщение</translation>
    </message>
</context>
<context>
    <name>verified::Update</name>
    <message>
        <source>Verified Plugin Update</source>
        <translation>Verified Plugin Обновление</translation>
    </message>
    <message>
        <source>&lt;big&gt;A new version of Verified Plugin is available.&lt;/big&gt;</source>
        <translation>&lt;big&gt;Доступна новая версия Verified Plugin!&lt;/big&gt;</translation>
    </message>
    <message>
        <source>Skip this version</source>
        <translation>Пропустить версию</translation>
    </message>
    <message>
        <source>Remind me later</source>
        <translation>Не сейчас</translation>
    </message>
    <message>
        <source>Install update</source>
        <translation>Установить</translation>
    </message>
    <message>
        <source>Verified Plugin %1 is now available (you have %2). Would you like to download it now? See changelog here: &lt;a href=&quot;http://vrp.im&quot;&gt;http://vrp.im&lt;/a&gt;</source>
        <translation>Verified Plugin %1 доступен для загрузки (у вас - %2). Загрузить обновление сейчас? Смотреть информацию об обновлении здесь: &lt;a href=&quot;http://vrp.im&quot;&gt;http://vrp.im&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>verified::Verified</name>
    <message>
        <source>Plugin not found at correct path. Please move it to %1.</source>
        <translation>Плагин находится не в том месте. Пожалуйста переместите его в %1.</translation>
    </message>
    <message>
        <source>Remind me in 1 hour</source>
        <translation>Напомнить через 1 час</translation>
    </message>
    <message>
        <source>Installed a new version.&lt;br/&gt;Need to restart application.</source>
        <translation>Установлена новая версия.&lt;br/&gt;Нужно перезапустить приложение.</translation>
    </message>
    <message>
        <source>&lt;big&gt;&lt;b&gt;Attention!&lt;/b&gt; You are communcating with person whose username looks like a Jabber ID but not identical with his real Jabber ID. Be careful!&lt;/big&gt;</source>
        <translation>&lt;big&gt;&lt;b&gt;Внимание!&lt;/b&gt; Вы общаетесь с человеком у которого никнейм похож на Jabber ID, но не соответствует его реальному Jabber ID. Будьте осторожны!&lt;/big&gt;</translation>
    </message>
    <message>
        <source>&lt;big&gt;&lt;b&gt;Attention!&lt;/b&gt; You are communcating with person who has blacks. Read here for details:&lt;br/&gt;%1&lt;/big&gt;</source>
        <translation>&lt;big&gt;&lt;b&gt;Внимание!&lt;/b&gt; Вы общаетесь с человеком на которого есть блеки. Подробнее об этом вы можете прочитать тут:&lt;br/&gt;%1&lt;/big&gt;</translation>
    </message>
    <message>
        <source>Verified Plugin Update</source>
        <translation>Verified Plugin Обновление</translation>
    </message>
    <message>
        <source>Verified forum</source>
        <translation>ресурс Verified</translation>
    </message>
    <message>
        <source>&lt;big&gt;&lt;b&gt;Attention!&lt;/b&gt; Just now you or your parthner send a link which contained in our fake forums database. Be careful! To known more open Verified forum and type in search box this URL.&lt;/big&gt;</source>
        <translation>&lt;big&gt;&lt;b&gt;Внимание!&lt;/b&gt; Только что вы или ваш собеседник указали ссылку, которая фигурирует в нашей базе фейковых форумов. Будьте осторожны! Для более подробной информации о данном фейке зайдите на ресурс Verified и вбейте в поиск данный URL.&lt;/big&gt;</translation>
    </message>
    <message>
        <source>&lt;big&gt;&lt;b&gt;Attention!&lt;/b&gt; You are communicating with person who has registration on Verified forum&lt;br&gt;Login: %1&lt;br&gt;Registration date: %2&lt;br&gt;Link to profile: &lt;font color=&quot;blue&quot;&gt;/member.php?u=%3&lt;/font&gt;&lt;/big&gt;</source>
        <translation>&lt;big&gt;&lt;b&gt;Внимание!&lt;/b&gt; Вы общаетесь с человеком который зарегестрирован на ресурсе Verified&lt;br&gt;Логин: %1&lt;br&gt;Дата регистрации: %2&lt;br&gt;Ссылка на профайл: &lt;font color=&quot;blue&quot;&gt;/member.php?u=%3&lt;/font&gt;&lt;/big&gt;</translation>
    </message>
    <message>
        <source>&lt;big&gt;&lt;b&gt;Attention!&lt;/b&gt; You are communcating with person who uses NON-Latin characters in his Jabber ID. Usually scammers do this. Read here &lt;font color=&quot;blue&quot;&gt;/showthread.php?t=7336&lt;/font&gt; (Verified forum) for details.&lt;/big&gt;</source>
        <translation>&lt;big&gt;&lt;b&gt;Внимание!&lt;/b&gt; Вы общаетесь с человеком который использует в своем Jabber ID НЕ латинские буквы. Обычно такое делают кидалы, подробнее о данном виде мошенничества можно прочитать тут: &lt;font color=&quot;blue&quot;&gt;/showthread.php?t=7336&lt;/font&gt; (ресурс Verified).&lt;/big&gt;</translation>
    </message>
</context>
<context>
    <name>verified::VerifiedOptions</name>
    <message>
        <source>Plugin must be at</source>
        <translation>Плагин должен быть здесь</translation>
    </message>
    <message>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation>Прокси</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <source>Username</source>
        <translatorcomment>Пароль</translatorcomment>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
</context>
</TS>
