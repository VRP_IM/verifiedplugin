#pragma once

#include <QDialog>

#include <QStringList>

namespace verified {

namespace Ui { class BlackList; }

class BlackList : public QDialog
{
    Q_OBJECT

public:
    explicit BlackList(const QStringList &urls, QWidget *parent = 0);
    ~BlackList();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::BlackList *ui;
};

} // namespace verified
