#include "update.h"
#include "ui_update.h"

namespace verified {

Update::Update(int newVersion, int oldVersion, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Update)
{
    ui->setupUi(this);

    ui->lbMessage->setText(tr("Verified Plugin %1 is now available (you have %2). Would you like to download it now? See changelog here: <a href=\"http://vrp.im\">http://vrp.im</a>")
                           .arg(QString::number(newVersion), QString::number(oldVersion)));
}

Update::~Update()
{
    delete ui;
}

void Update::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;

    default:
        break;
    }
}

void Update::skipVersion()
{
    done(SkipVersion);
}

void Update::remindLater()
{
    done(RemindLater);
}

void Update::installUpdate()
{
    done(InstallUpdate);
}

} // namespace verified
