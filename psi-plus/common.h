#pragma once

#if QT_VERSION >= 0x050000
# define QS(str) QStringLiteral(str)
# define QSS(str) QStringLiteral(str)
#else
# define QS(str) QLatin1String(str)
# define QSS(str) QString(QLatin1String(str))
#endif
#define QC(c) QLatin1Char(c)
