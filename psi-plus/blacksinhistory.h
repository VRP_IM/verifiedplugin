#pragma once

#include <QDialog>

#include <QMultiHash>

namespace verified {

namespace Ui {

class BlacksInHistory;

}

class BlacksInHistory : public QDialog
{
    Q_OBJECT

public:
    explicit BlacksInHistory(QWidget *parent = 0);
    ~BlacksInHistory();

    void setScammers(const QMultiHash<QString, QString> &scammers);

private:
    Ui::BlacksInHistory *ui;
};

} // namespace verified
