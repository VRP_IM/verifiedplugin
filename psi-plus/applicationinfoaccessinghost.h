#pragma once

#include <QObject>

#include <QVariant>
#include <QString>

// Class to workaround Psi+ ABI changes

class ApplicationInfoAccessingHost;
class OptionAccessingHost;

namespace verified {

struct Proxy {
    QString type;
    QString host;
    int port;
    QString user;
    QString pass;
};

class ApplicationInfoAccessingHost : public QObject
{
    Q_OBJECT

public:
    enum HomedirType { ConfigLocation, DataLocation, CacheLocation };

    ApplicationInfoAccessingHost(QObject *parent);

    void setNativeApplicationInfoAccessingHost(::ApplicationInfoAccessingHost *host);
    void setOptionAccessingHost(::OptionAccessingHost *host);

    // Version info
    QString appName();
    QString appVersion();

    // Directories
    QString appHomeDir(HomedirType type);
    QString appCurrentProfileDir(HomedirType type);
    QString appHistoryDir();

    Proxy getProxyFor(const QString &obj);

private:
    void readProfileName();
    ::ApplicationInfoAccessingHost *_appInfo;
    ::OptionAccessingHost *_optionHost;
    QString _profile;
};

} // namespace verified
