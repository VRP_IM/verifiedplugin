#include "blacklist.h"
#include "ui_blacklist.h"

#include "common.h"

namespace verified {

BlackList::BlackList(const QStringList &urls, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::BlackList)
{
    ui->setupUi(this);

    QString msg = tr("<b><font color=\"red\">Attention!</font></b> You got a BlackList. Please respond to it. Link to topic:");
    foreach (const QString &url, urls) {
        msg += QSS("<br/>%1").arg(url);
    }
    ui->lbMessage->setText(msg);
    adjustSize();
    setMinimumSize(size());
    setMaximumSize(size());
}

BlackList::~BlackList()
{
    delete ui;
}

void BlackList::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

} // namespace verified
