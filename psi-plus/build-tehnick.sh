#!/usr/bin/bash

set -x
set -e

compilers="i686 x86_64"
archs="win32 win64"

for c in $compilers; do
    [ -d $c ] && rm -fr $c

    includes="psi-plus psi-plus-1.4.1323"
    for i in $includes; do
        mkdir -p $c/$i
        plugins_root=$(pwd)/includes/$i

        pushd $c/$i
        $c-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DPLUGINS_ROOT_DIR=$plugins_root -DQT5_BUILD=ON ../..
        make -j5 VERBOSE=1
        popd
    done
done
