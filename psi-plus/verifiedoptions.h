#pragma once

#include "applicationinfoaccessinghost.h"

#include <optionaccessinghost.h>

#include <QWidget>
#include <QShowEvent>

namespace verified {

namespace Ui { class VerifiedOptions; }

class VerifiedOptions : public QWidget
{
    Q_OBJECT

public:
    explicit VerifiedOptions(QWidget *parent = 0);
    ~VerifiedOptions();

    void applyOptions();

    void setApplicationInfoAccessingHost(ApplicationInfoAccessingHost *appInfo);
    void setOptionAccessingHost(OptionAccessingHost *optionHost);

    void setPluginPath(const QString &path);
    void setSystem(const QString &system);

protected:
    void showEvent(QShowEvent *event);

private:
    Ui::VerifiedOptions *ui;
    ApplicationInfoAccessingHost *_appInfo;
    OptionAccessingHost *_optionHost;
};

} // namespace verified
