#include "applicationinfoaccessinghost.h"

#include "common.h"

#include <optionaccessinghost.h>

#include <QDir>
#include <QDebug>
#include <QCoreApplication>
#include <QSettings>

#if defined WIME || QT_VERSION < QT_VERSION_CHECK(5, 6, 0)
# define NATIVE_APP_INFO
#endif

#ifdef NATIVE_APP_INFO
# include <applicationinfoaccessinghost.h>
#else
# include <QStandardPaths>
# ifdef Q_OS_WIN
#  include <windows.h>
#  include <shlobj.h>
# endif
#endif

namespace verified {

#ifndef NATIVE_APP_INFO
# ifdef Q_OS_LINUX
static QString sname()
{
    return QS("psi+");
}
# endif
# ifdef Q_OS_WIN
static bool isPortable()
{
    static bool portable = QFileInfo(QCoreApplication::applicationFilePath()).fileName().toLower().indexOf("portable") != -1;
    return portable;
}
# endif
#endif

ApplicationInfoAccessingHost::ApplicationInfoAccessingHost(QObject *parent)
    : QObject(parent)
    , _appInfo()
    , _optionHost()
{
}

void ApplicationInfoAccessingHost::setNativeApplicationInfoAccessingHost(::ApplicationInfoAccessingHost *host)
{
#ifdef NATIVE_APP_INFO
    _appInfo = host;
#else
    Q_UNUSED(host)
#endif
}

void ApplicationInfoAccessingHost::setOptionAccessingHost(OptionAccessingHost *host)
{
    _optionHost = host;
}

QString ApplicationInfoAccessingHost::appName()
{
#ifdef NATIVE_APP_INFO
    return _appInfo->appName();
#else
    return qApp->applicationName();
#endif
}

QString ApplicationInfoAccessingHost::appVersion()
{
#ifdef NATIVE_APP_INFO
    return _appInfo->appVersion();
#else
    return qApp->applicationVersion();
#endif
}

QString ApplicationInfoAccessingHost::appHomeDir(ApplicationInfoAccessingHost::HomedirType type)
{
#ifdef NATIVE_APP_INFO
    return _appInfo->appHomeDir(static_cast< ::ApplicationInfoAccessingHost::HomedirType>(static_cast<int>(type)));
#else

    // Taken from original Psi+ code

    static QString configDir_;
    static QString dataDir_;
    static QString cacheDir_;

    if (configDir_.isEmpty()) {
        // Try the environment override first
        configDir_ = QString::fromLocal8Bit(qgetenv("PSIDATADIR"));

        if (configDir_.isEmpty()) {
# if defined (Q_OS_WIN)
            QString base = isPortable() ? QCoreApplication::applicationDirPath() : "";
            if (base.isEmpty()) {
                wchar_t path[MAX_PATH];
                if (SHGetFolderPathW(nullptr, CSIDL_APPDATA, nullptr, 0, path) == S_OK) {
                    configDir_ = QString::fromWCharArray(path) + "\\" + appName();
                }
                else {
                    configDir_ = QDir::homePath() + "/" + appName();
                }
                dataDir_ = configDir_;
                // prefer non-roaming data location for cache which is default for qds:DataLocation
                cacheDir_ = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
            }
            else {
                configDir_ = dataDir_ = cacheDir_ = base + "/" + appName();
            }
            // temporary store for later processing
            QDir configDir(configDir_);
            QDir cacheDir(cacheDir_);
            QDir dataDir(dataDir_);
# elif defined (Q_OS_MAC)
            QDir configDir(QDir::homePath() + "/Library/Application Support/" + appName());
            QDir cacheDir(QDir::homePath() + "/Library/Caches/" + appName());
            QDir dataDir(configDir);
# else

            QString XdgConfigHome(QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation));
            QString XdgDataHome(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation));
            QString XdgCacheHome(QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation));

            if (XdgConfigHome.isEmpty()) {
                XdgConfigHome = QDir::homePath() + "/.config";
            }
            if (XdgDataHome.isEmpty()) {
                XdgDataHome = QDir::homePath() + "/.local/share";
            }
            if (XdgCacheHome.isEmpty()) {
                XdgCacheHome = QDir::homePath() + "/.cache";
            }
            QDir configDir(XdgConfigHome + QS("/") + sname());
            QDir dataDir(XdgDataHome + QS("/") + sname());
            QDir cacheDir(XdgCacheHome + QS("/") + sname());

# endif
            configDir_ = configDir.path();
            cacheDir_  = cacheDir.path();
            dataDir_   = dataDir.path();

            if (!dataDir.exists()) {
                dataDir.mkpath(".");
            }
            if (!cacheDir.exists()) {
                cacheDir.mkpath(".");
            }
        }
        else {
            cacheDir_ = configDir_;
            dataDir_  = configDir_;
        }
    }

    QString ret;
    switch (type) {
    case ApplicationInfoAccessingHost::ConfigLocation:
        ret = configDir_;
        break;

    case ApplicationInfoAccessingHost::DataLocation:
        ret = dataDir_;
        break;

    case ApplicationInfoAccessingHost::CacheLocation:
        ret = cacheDir_;
        break;
    }
    return ret;
#endif
}

QString ApplicationInfoAccessingHost::appCurrentProfileDir(ApplicationInfoAccessingHost::HomedirType type)
{
    readProfileName();
    return appHomeDir(type) + QS("/profiles/") + _profile;
}

QString ApplicationInfoAccessingHost::appHistoryDir()
{
    return appCurrentProfileDir(DataLocation) + QS("/history");
}

Proxy ApplicationInfoAccessingHost::getProxyFor(const QString &obj)
{
    Proxy res;
#ifdef NATIVE_APP_INFO
    ::Proxy nativeProxy = _appInfo->getProxyFor(obj);
    res.type = nativeProxy.type;
    res.host = nativeProxy.host;
    res.port = nativeProxy.port;
    res.user = nativeProxy.user;
    res.pass = nativeProxy.pass;
#else
    Q_UNUSED(obj)
#endif

    // Prefer proxy options from plugin settings
    if (!_optionHost->getPluginOption(QS("proxy.host")).toString().trimmed().isEmpty()) {
        res.type = _optionHost->getPluginOption(QS("proxy.type")).toString();
        res.host = _optionHost->getPluginOption(QS("proxy.host")).toString();
        res.port = _optionHost->getPluginOption(QS("proxy.port")).toInt();
        res.user = _optionHost->getPluginOption(QS("proxy.user")).toString();
        res.pass = _optionHost->getPluginOption(QS("proxy.pass")).toString();
    }

    return res;
}

void ApplicationInfoAccessingHost::readProfileName()
{
    if (_profile.isEmpty()) {
        QSettings settings(appHomeDir(ConfigLocation) + QS("/psirc"), QSettings::IniFormat);
        _profile = settings.value(QS("last_profile"), QS("default")).toString();
    }
}

} // end namespace verified
