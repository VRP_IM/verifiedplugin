#!/bin/bash

otool -L libverifiedplugin.so | grep Qt | while read -a libs ; do
    install_name_tool -change $libs ${libs/@rpath/@executable_path\/..\/Frameworks} libverifiedplugin.so
done
