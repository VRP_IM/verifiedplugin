#pragma once

#include <QDialog>

namespace verified {

namespace Ui { class Update; }

class Update : public QDialog
{
    Q_OBJECT

public:
    explicit Update(int newVersion, int oldVersion, QWidget *parent = 0);
    ~Update();

    enum Result
    {
        SkipVersion,
        RemindLater,
        InstallUpdate
    };

protected:
    void changeEvent(QEvent *e);

private slots:
    void skipVersion();
    void remindLater();
    void installUpdate();

private:
    Ui::Update *ui;
};

} // namespace verified
