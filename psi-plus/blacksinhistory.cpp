#include "blacksinhistory.h"
#include "ui_blacksinhistory.h"

#include "common.h"

namespace verified {

BlacksInHistory::BlacksInHistory(QWidget *parent)
#ifdef Q_OS_WIN
    : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::MSWindowsFixedSizeDialogHint)
#else
    : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint)
#endif
    , ui(new Ui::BlacksInHistory)
{
    ui->setupUi(this);
}

BlacksInHistory::~BlacksInHistory()
{
    delete ui;
}

void BlacksInHistory::setScammers(const QMultiHash<QString, QString> &scammers)
{
    QString text;
    foreach (const QString &scammer, scammers.uniqueKeys()) {
        text += QS("<b>") + scammer + QS("</b><br>");
        QList<QString> list = scammers.values(scammer);
        QStringList threads;
        foreach (const QString &thread, list) {
            threads << thread;
        }
        text += threads.join(QS("<br>"));
        text += QS("<hr>");
    }

    ui->tbScammers->setHtml(text);
}

} // namespace verified
