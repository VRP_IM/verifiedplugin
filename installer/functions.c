#include "functions.h"

#include <windows.h>
#include <shlobj.h>
#include <stdio.h>
#include <wininet.h>

BOOL downloadFile(LPCTSTR url, LPCTSTR path)
{
    BOOL res = FALSE;

    HINTERNET hInternetSession;
    HINTERNET hURL;
    DWORD dwBytesRead = 1;

    // Make internet connection.
    hInternetSession = InternetOpen("tes", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);

    // Make connection to desired page.
    hURL = InternetOpenUrl(hInternetSession, url, NULL, 0, 0, 0);

    char buf[1024];

    DWORD dwTemp;
    HANDLE hFile = CreateFile(path, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    if (hFile != INVALID_HANDLE_VALUE) {
        res = TRUE;
        BOOL headerChecked = FALSE;
        while (dwBytesRead > 0) {
            InternetReadFile(hURL, buf, (DWORD)sizeof(buf), &dwBytesRead);
            WriteFile(hFile, buf, dwBytesRead, &dwTemp, NULL);

            if (!headerChecked) {
                headerChecked = TRUE;

                // Check for correct PE-file signature
                if (buf[0] != 'M' || buf[1] != 'Z') {
                    res = FALSE;
                    break;
                }
            }
        }
        CloseHandle(hFile);
    }

    // Close down connections.
    InternetCloseHandle(hURL);
    InternetCloseHandle(hInternetSession);

    return res;
}

BOOL directoryExists(LPCTSTR szPath)
{
    DWORD dwAttrib = GetFileAttributes(szPath);
    return (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

BOOL fileExists(LPCTSTR szPath)
{
    DWORD dwAttrib = GetFileAttributes(szPath);
    return dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY) == 0;
}

BOOL fileExistsInDir(LPCTSTR filename, LPCTSTR dirpath)
{
    char buf[MAX_PATH];
    sprintf(buf, "%s\\%s", dirpath, filename);
    return fileExists(buf);
}

int compVersions(int major1, int minor1, int bugfix1, int major2, int minor2, int bugfix2)
{
    if (major1 < major2) {
        return -1;
    }

    if (major1 > major2) {
        return 1;
    }

    if (minor1 < minor2) {
        return -1;
    }

    if (minor1 > minor2) {
        return 1;
    }

    if (bugfix1 < bugfix2) {
        return -1;
    }

    if (bugfix1 > bugfix2) {
        return 1;
    }

    return 0;
}

// LPCTSTR filename, LPCTSTR dirpath

// Returns TRUE if version of file is greater or equal than need
BOOL checkFileVersion(LPCTSTR filename, LPCTSTR dirpath, int major, int minor, int build)
{
    char szPath[MAX_PATH] = "";
    sprintf(szPath, "%s\\%s", dirpath, filename);
    BOOL res = TRUE;

    DWORD verHandle = 0;
    UINT size = 0;
    LPBYTE lpBuffer = NULL;
    DWORD verSize = GetFileVersionInfoSize(szPath, &verHandle);

    if (verSize != 0) {
        LPSTR verData = malloc(verSize);

        if (GetFileVersionInfo(szPath, verHandle, verSize, verData)) {
            if (VerQueryValue(verData, "\\", (VOID FAR* FAR*)&lpBuffer, &size)) {
                if (size) {
                    VS_FIXEDFILEINFO *verInfo = (VS_FIXEDFILEINFO*)(void*)lpBuffer;
                    if (verInfo->dwSignature == 0xfeef04bd) {
                        int fileMajor = (verInfo->dwFileVersionMS >> 16) & 0xffff;
                        int fileMinor = (verInfo->dwFileVersionMS >> 0) & 0xffff;
                        int fileBuild = (verInfo->dwFileVersionLS >> 16) & 0xffff;
                        res = compVersions(fileMajor, fileMinor, fileBuild, major, minor, build) > -1 ? TRUE : FALSE;
                        printf("Psi+ %d.%d.%d - %d\n", fileMajor, fileMinor, fileBuild, res);
                    }
                }
            }
        }
        free(verData);
    }

    return res;
}

void genAppId(App *app)
{
    char buf[2000] = "";

    strcat(buf, app->appName);

    if (app->version) {
        strcat(buf, "-");
        strcat(buf, app->version);
    }

    strcat(buf, "-windows");
    if (app->compiler) {
        strcat(buf, "_");
        strcat(buf, app->compiler);
    }

    if (app->arch) {
        strcat(buf, "-");
        strcat(buf, app->arch);
    }

    app->appId = strdup(buf);
}

void genUrl(App *app)
{
    char buf[2000] = "http://vrp.im/";

    strcat(buf, app->appId);
    strcat(buf, "/");
    strcat(buf, app->pluginName);

    app->url = strdup(buf);
}

App *detectApp(LPCTSTR appPath)
{
    App *res = malloc(sizeof(App));
    memset(res, 0, sizeof(App));

    char buf[MAX_PATH];
    char appDataDir[MAX_PATH];

    SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_DEFAULT, appDataDir);

    // Pidgin
    if (!res->appName && fileExistsInDir("pidgin.exe", appPath)) {
        sprintf(buf, "%s\\.purple\\plugins", appDataDir);

        res->appName = strdup("pidgin");
        res->arch = strdup("x32");
        res->pluginName = strdup("verified.dll");
        res->pluginDir = strdup(buf);
    }

    // Portable Wime
    sprintf(buf, "%s\\bin", appPath);
    if (!res->appName && fileExistsInDir("Wime.exe", buf)) {
        sprintf(buf, "%s\\userdata\\Wime\\plugins", appPath);

        res->appName = strdup("wime");
        res->compiler = strdup("mingw");
        res->arch = strdup("x32");
        res->pluginName = strdup("libverifiedplugin.dll");
        res->pluginDir = strdup(buf);
    }

    // Wime
    sprintf(buf, "%s\\Wime.exe", appPath);
    if (!res->appName && fileExistsInDir("Wime.exe", appPath)) {
        sprintf(buf, "%s\\Wime\\plugins", appDataDir);

        res->appName = strdup("wime");
        res->compiler = strdup("mingw");
        res->arch = strdup("x32");
        res->pluginName = strdup("libverifiedplugin.dll");
        res->pluginDir = strdup(buf);
    }

    // Psi+ everthing
    if (!res->appName) {
        // Psi+ Qt 4.8.5
        if (fileExistsInDir("mingwm10.dll", appPath)) {
            sprintf(buf, "%s\\Psi+\\plugins", appDataDir);

            res->appName = strdup("psi+");
            res->compiler = strdup("mingw_4_8_5");
            res->arch = strdup("x32");
            res->pluginName = strdup("libverifiedplugin.dll");
            res->pluginDir = strdup(buf);
        }

        // Portable Psi+ Qt5 Tehnick or Psi+ Qt5 x32 Kukuruzo
        if (!res->appName) {
            // Detect MinGW architecture
            if (fileExistsInDir("libgcc_s_sjlj-1.dll", appPath)) {
                res->arch = strdup("x32");
            }
            else if (fileExistsInDir("libgcc_s_seh-1.dll", appPath)) {
                res->arch = strdup("x64");
            }
        }

        if (res->arch) {
            // Check for portable
            char *exeName = NULL;
            sprintf(buf, "%s\\psi-plus-portable.exe", appPath);
            if (fileExistsInDir("psi-plus-portable.exe", appPath)) {
                exeName = "psi-plus-portable.exe";
                sprintf(buf, "%s\\Psi+\\plugins", appPath);
            }
            else if (fileExistsInDir("psi-plus.exe", appPath)) {
                exeName = "psi-plus.exe";
                sprintf(buf, "%s\\Psi+\\plugins", appDataDir);
            }

            if (exeName) {
                res->pluginDir = strdup(buf);
                res->appName = strdup("psi+");
                res->compiler = strdup("mingw");
                res->pluginName = strdup("libverifiedplugin.dll");
                if (checkFileVersion(exeName, appPath, 1, 4, 1323)) {
                    res->version = strdup("1.4.1323");
                }
            }
        }

        // Psi+ Qt 5.5.1
        if (!res->appName && fileExistsInDir("libgcc_s_dw2-1.dll", appPath)) {
            sprintf(buf, "%s\\Psi+\\plugins", appDataDir);

            res->arch = strdup("x32");
            res->compiler = strdup("mingw_5_5_1");
            res->pluginName = strdup("libverifiedplugin.dll");
            res->pluginDir = strdup(buf);
        }

        // Psi+ x64 Qt MSVC Kukuruzo
        if (!res->appName && fileExistsInDir("psi-plus.exe", appPath)) {
            sprintf(buf, "%s\\Psi+\\plugins", appDataDir);

            res->appName = strdup("psi+");
            res->arch = strdup("x64");
            res->compiler = strdup("msvc");
            res->pluginName = strdup("verifiedplugin.dll");
            res->pluginDir = strdup(buf);
            if (checkFileVersion("psi-plus.exe", appPath, 1, 4, 1323)) {
                res->version = strdup("1.4.1323");
            }
        }
    }

    if (!res->appName || !res->arch || !res->pluginName || !res->pluginDir) {
        free(res->appName);
        free(res->version);
        free(res->compiler);
        free(res->arch);
        free(res->url);
        free(res->pluginName);
        free(res->pluginDir);
        free(res->pluginPath);
        free(res->appId);
        free(res);
        res = NULL;
    }

    if (res) {
        genAppId(res);
        genUrl(res);
        sprintf(buf, "%s\\%s", res->pluginDir, res->pluginName);
        res->pluginPath = strdup(buf);
    }

    return res;
}
