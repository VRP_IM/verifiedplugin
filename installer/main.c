#include "functions.h"

#include <windows.h>
#include <shlobj.h>
#include <stdio.h>
#include <wininet.h>

#define IDLABEL 501
#define IDPATH 502
#define IDBUTTON 503
#define IDINSTALL 504
#define IDDONE 505

// Found app info
#define IDAPPLABEL 506
#define IDAPP 507
#define IDURLLABEL 508
#define IDURL 509
#define IDPLUGINPATHLABEL 510
#define IDPLUGINPATH 511


CALLBACK LRESULT windowProcedure( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    (void)hPrevInstance;
    (void)lpCmdLine;

    const char wincClassName[] = "VerifiedWindow";
    WNDCLASSEX wc;
    MSG msg;

    wc.lpszClassName = wincClassName;
    wc.lpfnWndProc = windowProcedure;
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style  = 0;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc.lpszMenuName = NULL;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wc)) {
        MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    int width = 700, height = 300;
    int posLeft = 400, posTop = 100;

    HWND hwnd = CreateWindow(
                    wc.lpszClassName,
                    "Install Verified plugin",
                    WS_OVERLAPPEDWINDOW,
                    posLeft,
                    posTop,
                    width,
                    height,
                    NULL,
                    NULL,
                    hInstance,
                    NULL
                    );

    if(hwnd == NULL){
        MessageBox(NULL, "Error: Failure to create Window", "Error Report", MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    int y = 10;
    int h = 22;
    int x = 10;
    int w = 500;
    CreateWindow("static", "Choose Psi+, Wime or Pidgin folder:", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDLABEL, hInstance, NULL);

    y += h + 5;
    w = 450;
    CreateWindow("edit", "", WS_CHILD | WS_VISIBLE | ES_LEFT | WS_BORDER, x, y, w, h, hwnd, (HMENU)IDPATH, hInstance, NULL);

    x += w + 10;
    w = 80;
    CreateWindow("button", "Browse...", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDBUTTON, hInstance, NULL);

    x += w + 10;
    w = 80;
    CreateWindow("button", "Install", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDINSTALL, hInstance, NULL);

    x = 10;
    y += h + 5;
    w = 1000;
    CreateWindow("static", "", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDDONE, hInstance, NULL);

    // App info

    x = 10;
    y += h + 5;
    w = 100;
    CreateWindow("static", "Application", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDAPPLABEL, hInstance, NULL);

    x += w + 5;
    w = 1000;
    CreateWindow("edit", "", WS_CHILD | WS_VISIBLE | ES_LEFT | WS_BORDER | ES_READONLY, x, y, w, h, hwnd, (HMENU)IDAPP, hInstance, NULL);

    x = 10;
    y += h + 5;
    w = 100;
    CreateWindow("static", "Url", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDURLLABEL, hInstance, NULL);

    x += w + 5;
    w = 1000;
    CreateWindow("edit", "", WS_CHILD | WS_VISIBLE | ES_LEFT | WS_BORDER | ES_READONLY, x, y, w, h, hwnd, (HMENU)IDURL, hInstance, NULL);

    x = 10;
    y += h + 5;
    w = 100;
    CreateWindow("static", "Plugin path", WS_CHILD | WS_VISIBLE, x, y, w, h, hwnd, (HMENU)IDPLUGINPATHLABEL, hInstance, NULL);

    x += w + 5;
    w = 1000;
    CreateWindow("edit", "", WS_CHILD | WS_VISIBLE | ES_LEFT | WS_BORDER | ES_READONLY, x, y, w, h, hwnd, (HMENU)IDPLUGINPATH, hInstance, NULL);


    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    while(GetMessage(&msg, NULL, 0, 0) > 0 ){
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

char *browseFolder()
{
    char path[MAX_PATH];
    char *res = NULL;

    BROWSEINFO bi = {0};
    bi.lpszTitle = "Browse for folder...";
    bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;

    LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

    if (pidl != 0) {
        //get the name of the folder and put it in path
        SHGetPathFromIDList(pidl, path);
        res = strdup(path);
    }

    return res;
}

CALLBACK LRESULT windowProcedure(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg) {
    case WM_CREATE:
        break;

    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_MOVE:
        break;

    case WM_SIZE:
        if (wParam == SIZE_RESTORED) {
            int width = (int)LOWORD(lParam);

            RECT rect;
            POINT point;

            GetWindowRect(GetDlgItem(hwnd, IDDONE), &rect);
            point.x = rect.left;
            point.y = rect.top;
            ScreenToClient(hwnd, &point);
            MoveWindow(GetDlgItem(hwnd, IDDONE), point.x, point.y, width - point.x - 10, rect.bottom - rect.top, TRUE);

            GetWindowRect(GetDlgItem(hwnd, IDAPP), &rect);
            point.x = rect.left;
            point.y = rect.top;
            ScreenToClient(hwnd, &point);
            MoveWindow(GetDlgItem(hwnd, IDAPP), point.x, point.y, width - point.x - 10, rect.bottom - rect.top, TRUE);

            GetWindowRect(GetDlgItem(hwnd, IDURL), &rect);
            point.x = rect.left;
            point.y = rect.top;
            ScreenToClient(hwnd, &point);
            MoveWindow(GetDlgItem(hwnd, IDURL), point.x, point.y, width - point.x - 10, rect.bottom - rect.top, TRUE);

            GetWindowRect(GetDlgItem(hwnd, IDPLUGINPATH), &rect);
            point.x = rect.left;
            point.y = rect.top;
            ScreenToClient(hwnd, &point);
            MoveWindow(GetDlgItem(hwnd, IDPLUGINPATH), point.x, point.y, width - point.x - 10, rect.bottom - rect.top, TRUE);
        }
        return 0;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDBUTTON) {
            char *path = browseFolder();
            if (path) {
                SetWindowText(GetDlgItem(hwnd, IDPATH), path);
                App *app = detectApp(path);

                if (app) {
                    SetWindowText(GetDlgItem(hwnd, IDDONE), "");
                    SetWindowText(GetDlgItem(hwnd, IDAPP), app->appId);
                    SetWindowText(GetDlgItem(hwnd, IDURL), app->url);
                    SetWindowText(GetDlgItem(hwnd, IDPLUGINPATH), app->pluginPath);
                }
                else {
                    SetWindowText(GetDlgItem(hwnd, IDDONE), "Psi+, Wime or Pidgin not found. Need to choose a folder where Psi+, Wime or Pidgin installed!");
                    SetWindowText(GetDlgItem(hwnd, IDAPP), "");
                    SetWindowText(GetDlgItem(hwnd, IDURL), "");
                    SetWindowText(GetDlgItem(hwnd, IDPLUGINPATH), "");
                }

                free(path);
            }
        }
        else if (LOWORD(wParam) == IDINSTALL) {
            SetWindowText(GetDlgItem(hwnd, IDDONE), "");
            char path[MAX_PATH];
            GetWindowText(GetDlgItem(hwnd, IDPATH), path, sizeof(path) - 1);

            if (strlen(path)) {
                App *app = detectApp(path);
                if (app) {
                    if (!directoryExists(app->pluginDir)) {
                        CreateDirectory(app->pluginDir, NULL);
                    }

                    if (downloadFile(app->url, app->pluginPath)) {
                        SetWindowText(GetDlgItem(hwnd, IDDONE), "Success!");
                    }
                    else {
                        SetWindowText(GetDlgItem(hwnd, IDDONE), "Can't download plugin!");
                    }
                }
                else {
                    SetWindowText(GetDlgItem(hwnd, IDDONE), "Psi+, Wime or Pidgin not found. Need to choose a folder where Psi+, Wime or Pidgin installed!");
                }
            }
        }
        break;

    default:
        return DefWindowProc(hwnd, msg, wParam, lParam);
    }

    return 0;
}
