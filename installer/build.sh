#!/usr/bin/bash

set -e
set -x

[ -f verifiedplugin.zip ] && rm verifiedplugin.zip
[ -f verifiedplugin-setup.exe ] && rm verifiedplugin-setup.exe

zip -r verifiedplugin.zip           \
    psi+-windows_mingw_5_5_1-x32    \
    psi+-windows_mingw_4_8_5-x32    \
    psi+-windows_mingw-x32          \
    psi+-windows_mingw-x64          \
    psi+-1.4.1323-windows_mingw-x32 \
    psi+-1.4.1323-windows_mingw-x64 \
    psi+-windows_msvc-x64           \
    psi+-1.4.1323-windows_msvc-x64  \
    wime-windows_mingw-x32          \
    pidgin-windows-x32              \
    wime-macos                      \
    psi+-macos                      \
    -i '*.dll' -i '*.so'

[ -d release ] && rm -fr release
mkdir release
pushd release
cmake -DCMAKE_BUILD_TYPE=Release -G Ninja ..
ninja
popd
