#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <windows.h>

typedef struct
{
    char *appName;
    char *version;
    char *compiler;
    char *arch;
    char *url;
    char *pluginName;
    char *pluginDir;
    char *pluginPath;
    char *appId;
} App;

BOOL downloadFile(LPCTSTR url, LPCTSTR path);
BOOL directoryExists(LPCTSTR szPath);
App *detectApp(LPCTSTR appPath);
BOOL copyPlugin(LPCTSTR appPath);

#endif // FUNCTIONS_H
